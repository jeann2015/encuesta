<?php

use Illuminate\Http\Request;
use App\Eventos;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');


Route::get('/tipoevento/preguntas/{id}', function (Request $request) {

    $preguntas = \DB::table('preguntas')
        ->where('id_tipo_evento','=',$request->id)
        ->pluck('description', 'id');

    return response()->json($preguntas);

});

Route::get('/graph/evento/general/reports/{id}', function (Request $request)
{
    $e = new Eventos;
    $data = $e->EventPromedioResumenCerradas($request->id);
    return response()->json($data);
});

Route::get('/graph/evento/reports/general/norespondio/{id}', function (Request $request)
{
    $e = new Eventos;
    $data = $e->GetParticipantes($request->id);
    return response()->json($data);
});

Route::get('/graph/evento/reports/general/promedio/{id}', function (Request $request)
{
    $e = new Eventos;
    $data = $e->GetPromedioNeto($request->id);
    return response()->json($data);
});

Route::get('graph/evento/reports/preguntas/{id}', function (Request $request)
{
    $e = new Eventos;
    $data = $e->GetDatosEventosPreguntas($request->id);
    return response()->json($data);
});

Route::get('graph/evento/reports/eventos/{id}/preguntas/{idpre}', function (Request $request)
{


    $e = new Eventos;
    $data = $e->GetDatosEventosPreguntasRespuestas($request->id,$request->idpre);
    dd($data);
    return response()->json($data);
});

