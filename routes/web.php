<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/modules', 'ModulesController@index');
Route::get('/modules/add', 'ModulesController@add');
Route::post('/modules/new', 'ModulesController@news');
Route::get('/modules/edit/{id}', 'ModulesController@edit');
Route::post('/modules/update', 'ModulesController@update');
Route::get('/modules/delete/{id}', 'ModulesController@delete');
Route::post('/modules/destroy', 'ModulesController@destroy');

Route::get('/audit', 'AuditsController@index');

Route::get('/home', 'HomeController@index');
Route::get('/users', 'UsersController@index');
Route::get('/users/add', 'UsersController@add');
Route::post('/users/new', 'UsersController@news');
Route::get('/users/edit/{id}', 'UsersController@edit');
Route::post('/users/update', 'UsersController@update');
Route::get('/users/delete/{id}', 'UsersController@delete');
Route::post('/users/destroy', 'UsersController@destroy');

Route::get('/grupos', 'GruposController@index');
Route::get('/grupos/add', 'GruposController@add');
Route::post('/grupos/new', 'GruposController@news');
Route::get('/grupos/edit/{id}', 'GruposController@edit');
Route::post('/grupos/update', 'GruposController@update');
Route::get('/grupos/delete/{id}', 'GruposController@delete');
Route::post('/grupos/destroy', 'GruposController@destroy');

Route::get('/pais', 'PaisController@index');
Route::get('/pais/add', 'PaisController@add');
Route::post('/pais/new', 'PaisController@news');
Route::get('/pais/edit/{id}', 'PaisController@edit');
Route::post('/pais/update', 'PaisController@update');
Route::get('/pais/delete/{id}', 'PaisController@delete');
Route::post('/pais/destroy', 'PaisController@destroy');

Route::get('/soluciones', 'SolucionesController@index');
Route::get('/soluciones/add', 'SolucionesController@add');
Route::post('/soluciones/new', 'SolucionesController@news');
Route::get('/soluciones/edit/{id}', 'SolucionesController@edit');
Route::post('/soluciones/update', 'SolucionesController@update');
Route::get('/soluciones/delete/{id}', 'SolucionesController@delete');
Route::post('/soluciones/destroy', 'SolucionesController@destroy');


Route::post('/solucionestipos/new', 'SolucionesTiposController@news');
Route::get('/solucionestipos/edit/{id}/{id_sol}', 'SolucionesTiposController@edit');
Route::post('/solucionestipos/update', 'SolucionesTiposController@update');
Route::get('/solucionestipos/delete/{id}/{id_sol}', 'SolucionesTiposController@delete');
Route::post('/solucionestipos/destroy', 'SolucionesTiposController@destroy');

Route::get('/industrias', 'IndustriasController@index');
Route::get('/industrias/add', 'IndustriasController@add');
Route::post('/industrias/new', 'IndustriasController@news');
Route::get('/industrias/edit/{id}', 'IndustriasController@edit');
Route::post('/industrias/update', 'IndustriasController@update');
Route::get('/industrias/delete/{id}', 'IndustriasController@delete');
Route::post('/industrias/destroy', 'IndustriasController@destroy');

Route::get('/preguntas', 'PreguntasController@index');
Route::get('/preguntas/add', 'PreguntasController@add');
Route::post('/preguntas/new', 'PreguntasController@news');
Route::get('/preguntas/edit/{id}', 'PreguntasController@edit');
Route::post('/preguntas/update', 'PreguntasController@update');
Route::get('/preguntas/delete/{id}', 'PreguntasController@delete');
Route::post('/preguntas/destroy', 'PreguntasController@destroy');


Route::get('/preguntastipo/add/{idtipoevento}', 'PreguntasController@addtipo');
Route::post('/preguntastipo/new', 'PreguntasController@newstipo');
Route::get('/preguntastipo/edit/{id}/{idtipoevento}', 'PreguntasController@edittipo');
Route::post('/preguntastipo/update', 'PreguntasController@updatetipo');
Route::get('/preguntastipo/delete/{id}/{idtipoevento}', 'PreguntasController@deletetipo');
Route::post('/preguntastipo/destroy', 'PreguntasController@destroytipo');


Route::get('/respuestas', 'RespuestasController@index');
Route::get('/respuestas/add', 'RespuestasController@add');
Route::post('/respuestas/new', 'RespuestasController@news');
Route::get('/respuestas/edit/{id}', 'RespuestasController@edit');
Route::post('/respuestas/update', 'RespuestasController@update');
Route::get('/respuestas/delete/{id}', 'RespuestasController@delete');
Route::post('/respuestas/destroy', 'RespuestasController@destroy');

Route::get('/facilitador', 'FacilitadorController@index');
Route::get('/facilitador/add', 'FacilitadorController@add');
Route::post('/facilitador/new', 'FacilitadorController@news');
Route::get('/facilitador/edit/{id}', 'FacilitadorController@edit');
Route::post('/facilitador/update', 'FacilitadorController@update');
Route::get('/facilitador/delete/{id}', 'FacilitadorController@delete');
Route::post('/facilitador/destroy', 'FacilitadorController@destroy');

Route::get('/eventos', 'EventosController@index');
Route::get('/eventos/add', 'EventosController@add');
Route::post('/eventos/new', 'EventosController@news');
Route::get('/eventos/edit/{id}', 'EventosController@edit');
Route::post('/eventos/update', 'EventosController@update');
Route::get('/eventos/delete/{id}', 'EventosController@delete');
Route::post('/eventos/destroy', 'EventosController@destroy');

Route::get('/eventos/ver', 'EventosController@ver');
Route::post('/buscar/encuesta', 'EventosController@encuesta');
Route::get('/encuesta', 'EventosController@general');
Route::post('/eventos/encuesta', 'EventosController@encuestasave');

Route::get('/tipoeventos', 'TipoEventosController@index');
Route::get('/tipoeventos/add', 'TipoEventosController@add');
Route::post('/tipoeventos/new', 'TipoEventosController@news');
Route::get('/tipoeventos/edit/{id}', 'TipoEventosController@edit');
Route::post('/tipoeventos/update', 'TipoEventosController@update');
Route::get('/tipoeventos/delete/{id}', 'TipoEventosController@delete');
Route::post('/tipoeventos/destroy', 'TipoEventosController@destroy');

Route::get('/rconsolidado', 'ReportsController@consolidado');
Route::post('/rconsolidado/search', 'ReportsController@consearch');

Route::get('/rperiodo', 'ReportsController@periodo');
Route::post('/rperiodo/search', 'ReportsController@persearch');

Route::get('/respecifico', 'ReportsController@especifico');
Route::post('/respecifico/search', 'ReportsController@espsearch');
Route::get('/respecifico/ver/report/{id}', 'ReportsController@espver');
Route::post('/respecifico/ver/report/graph/', 'ReportsController@espgraph');

Auth::routes();

Route::get('/home', 'HomeController@index');
