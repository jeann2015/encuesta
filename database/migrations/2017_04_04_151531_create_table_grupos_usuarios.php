<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGruposUsuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('grupos_usuarios', function (Blueprint $table)
      {
          $table->increments('id');
          $table->integer('id_user')->unsigned();
          $table->integer('id_grupos')->unsigned();
          $table->timestamps();

          $table->foreign('id_user')->references('id')->on('users');
          $table->foreign('id_grupos')->references('id')->on('grupos');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('grupos_usuarios');
    }
}
