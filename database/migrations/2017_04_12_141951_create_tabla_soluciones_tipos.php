<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablaSolucionesTipos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('soluciones_tipos', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_soluciones')->unsigned();
            $table->string('description',100);
            $table->timestamps();

            $table->foreign('id_soluciones')->references('id')->on('soluciones');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('soluciones_tipos');
    }
}
