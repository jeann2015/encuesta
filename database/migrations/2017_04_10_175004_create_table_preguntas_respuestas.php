<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePreguntasRespuestas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preguntas_respuestas', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_preguntas')->unsigned();
            $table->integer('id_respuestas')->unsigned();
            $table->timestamps();

            $table->foreign('id_respuestas')->references('id')->on('respuestas');
            $table->foreign('id_preguntas')->references('id')->on('preguntas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preguntas_respuestas');
    }
}
