<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePreguntas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('preguntas', function (Blueprint $table) {
            $table->string('tipo',15)->after('description');
            $table->smallInteger('orden')->after('tipo');
            $table->Integer('id_tipo_evento')->unsigned()->after('orden');

            $table->foreign('id_tipo_evento')->references('id')->on('tipo_evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
