<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventosPreguntasRespuestasAbiertas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos_preguntas_respuestas_abiertas', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_eventos')->unsigned();
            $table->integer('id_preguntas')->unsigned();
            $table->text('respuesta');
            $table->timestamps();

            $table->foreign('id_eventos')->references('id')->on('eventos');
            $table->foreign('id_preguntas')->references('id')->on('preguntas');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos_preguntas_respuestas_abiertas');
    }
}
