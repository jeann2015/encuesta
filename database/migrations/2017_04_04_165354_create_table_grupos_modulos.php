<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableGruposModulos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('grupos_modules', function (Blueprint $table)
      {
          $table->increments('id');
          $table->integer('id_grupos')->unsigned();
          $table->integer('id_modules')->unsigned();
          $table->tinyInteger('views');
          $table->tinyInteger('inserts');
          $table->tinyInteger('modifys');
          $table->tinyInteger('deletes');  
          $table->timestamps();

          $table->foreign('id_modules')->references('id')->on('modules');
          $table->foreign('id_grupos')->references('id')->on('grupos');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::dropIfExists('grupos_modules');
    }
}
