<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEvento extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_user')->unsigned();
            $table->integer('id_pais')->unsigned();
            $table->integer('id_industrias')->unsigned();
            $table->integer('id_solucion')->unsigned();
            $table->integer('id_facilitador')->unsigned();
            $table->integer('id_tipo_evento')->unsigned();
            $table->string('organizacion', 100);
            $table->string('lugar', 100);
            $table->timestamps();

            $table->foreign('id_user')->references('id')->on('users');
            $table->foreign('id_pais')->references('id')->on('pais');
            $table->foreign('id_industrias')->references('id')->on('industrias');
            $table->foreign('id_solucion')->references('id')->on('soluciones');
            $table->foreign('id_facilitador')->references('id')->on('facilitador');
            $table->foreign('id_tipo_evento')->references('id')->on('tipo_evento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos');
    }
}
