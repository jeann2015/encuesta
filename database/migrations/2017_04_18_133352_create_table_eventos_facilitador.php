<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEventosFacilitador extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventos_facilitador', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('id_eventos')->unsigned();
            $table->integer('id_facilitador')->unsigned();
            $table->timestamps();

            $table->foreign('id_eventos')->references('id')->on('eventos');
            $table->foreign('id_facilitador')->references('id')->on('facilitador');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventos_facilitador');
    }
}
