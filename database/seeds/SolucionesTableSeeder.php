<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SolucionesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $solutions = array(
            "Liderazgo",
            "Ejecución",
            "Productividad",
            "Confianza",
            "La lealtad del cliente",
            "Rendimiento de las ventas",
            "Educación",
            "Gobierno / Aplicación de la ley"
        );

        $Leaderships = array(
            "Los 7 Hábitos de la Gente Altamente Efectiva",
            "Los 7 hábitos para los gerentes",
            "La Implementación del Líder de los 7 Hábitos",
            "Los 7 Hábitos: Fundaciones",
            "Acumen del negocio del edificio",
            "Liderazgo: Grandes Líderes, Grandes Equipos, Grandes Resultados",
            "Liderazgo: Serie modular",
            "Fundaciones de Liderazgo"
        );

        $Execution = array(
            "Las 4 Disciplinas del Sistema Operativo",
            "Las 4 Disciplinas de Ejecución"
        );

        $Productivity = array(
            "Las 5 opciones para la productividad extraordinaria",
            "Fundamentos de Gestión de Proyectos",
            "Presentación Ventaja",
            "Fundamentos de la gestión del tiempo",
            "Fundamentos de la gestión del tiempo",
            "Gestión del tiempo para Microsoft Outlook"
        );

        $Trust = array(
            "Liderando a la velocidad de la confianza",
            "La Velocidad de las Fundaciones de Confianza",
            "Confianza inteligente"
        );

        $CustomerLoyalty = array(
            "Sistema Promotor Neto",
            "Leading Customer Loyalty",
            "La lealtad y el compromiso de los empleados",
            "Ganar la lealtad del cliente"
        );

        $SalesPerformance = array(
            "Liderazgo de Ventas Boot Camp",
            "Ayudar a los clientes a tener éxito: llenando su tubería",
            "Ayudar a los clientes a tener éxito: oportunidades de calificación",
            "Ayudar a los clientes a tener éxito: cerrar la venta",
            "Planificación y ejecución de cuentas",
            "Entrenamiento",
            "Negociación",
            "Certificación Virtual"
        );

        $Education = array(
            "El líder en mí",
            "Los 7 Hábitos de Adolescentes Muy Eficaces",
            "Personal de desarrollo",
            "Los 7 Hábitos de Estudiantes Universitarios Altamente Efectivos"
        );

        $GovernmentLawEnforcement = array(
            "Los 7 Hábitos para las Familias de la Fuerza Aérea",
            "Los 7 Hábitos para las Familias del Ejército",
            "Los 7 Hábitos para las Familias de la Armada",
            "Grandes Líderes, para el Sector Público",
            "Liderazgo centrado en la diversidad para la aplicación de la ley",
            "Vigilancia a la velocidad de la confianza",
            "Grandes líderes ... para la aplicación de la ley",
            "Los 7 Hábitos para la Aplicación de la Ley"
        );

        $con=1;
        foreach ($solutions as $solution)
        {

            DB::table('soluciones')->insert([
                'description' => $solution,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

            foreach ($Leaderships as $Leadership)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $Leadership,
                    'id_soluciones' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($Execution as $Executio)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $Executio,
                    'id_soluciones' => 2,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($Productivity as $Productivit)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $Productivit,
                    'id_soluciones' => 3,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($Trust as $Trus)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $Trus,
                    'id_soluciones' => 4,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($CustomerLoyalty as $CustomerLoyalt)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $CustomerLoyalt,
                    'id_soluciones' => 5,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($SalesPerformance as $SalesPerformanc)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $SalesPerformanc,
                    'id_soluciones' => 6,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($Education as $Educatio)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $Educatio,
                    'id_soluciones' => 7,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }

            foreach ($GovernmentLawEnforcement as $GovernmentLawEnforcemen)
            {
                DB::table('soluciones_tipos')->insert([
                    'description' => $GovernmentLawEnforcemen,
                    'id_soluciones' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);
            }



    }
}
