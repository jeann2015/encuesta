<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PreguntasRespuestasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con = 1; $con <= 11; $con++) {
            DB::table('preguntas_respuestas')->insert([
                'id_preguntas' => 1,
                'id_respuestas' => $con,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            DB::table('preguntas_respuestas')->insert([
                'id_preguntas' => 2,
                'id_respuestas' => $con,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            DB::table('preguntas_respuestas')->insert([
                'id_preguntas' => 3,
                'id_respuestas' => $con,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }
    }
}
