<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class PaisTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pais_list = array(
            array('pais'=>"Costa Rica",
            'ini'=>'CR'),
            array('pais'=>"Panamá",
                'ini'=>'Pan'),
            array('pais'=>"Nicaragua",
                'ini'=>'Nic'),
            array('pais'=>"México",
                'ini'=>'Mex'),
            array('pais'=>"El Salvador",
                'ini'=>'Es'),
            array('pais'=>"Honduras",
                'ini'=>'Hon'),
            array('pais'=>"Guatemala",
                'ini'=>'Gua'),
            array('pais'=>"Chile",
                'ini'=>'Ch'),

        );

        foreach ($pais_list as $pais)
        {
            DB::table('pais')->insert([
                'description' => $pais['pais'],
                'iniciales' => $pais['ini'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
