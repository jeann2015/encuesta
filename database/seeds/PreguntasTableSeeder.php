<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;


class PreguntasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $taller_list = array(
            array(
            'pregunta'=>'¿Qué esperaba lograr con su participación en este taller?',
            'tipo'=>'Abierta',
            'evento'=>1
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>1
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted aplique lo que aprendió?',
                'tipo'=>'Cerrada',
                'evento'=>1
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este facilitador a otros?',
                'tipo'=>'Cerrada',
                'evento'=>1
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>1
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>1
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó del taller?',
                'tipo'=>'Abierta',
                'evento'=>1
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar de este taller?',
                'tipo'=>'Abierta',
                'evento'=>1
            ),

        );

        $proceso_list = array(
            array(
                'pregunta'=>'¿Qué esperaba lograr como resultado de este proceso?',
                'tipo'=>'Abierta',
                'evento'=>2
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este proceso a un cliente o colega?',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted aplique lo que aprendió?',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este consultor a otros?',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>2
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó del proceso?',
                'tipo'=>'Abierta',
                'evento'=>2
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar ?',
                'tipo'=>'Abierta',
                'evento'=>2
            ),

        );

        $aca_list = array(
            array(
                'pregunta'=>'¿Qué esperaba lograr como resultado de ésta academia?',
                'tipo'=>'Abierta',
                'evento'=>3
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende ésta academia a un cliente o colega?',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted aplique lo que aprendió?',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este facilitador a otros?',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>3
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó de esta academia?',
                'tipo'=>'Abierta',
                'evento'=>3
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar ?',
                'tipo'=>'Abierta',
                'evento'=>3
            ),

        );

        $sesion_list = array(
            array(
                'pregunta'=>'¿Qué esperaba lograr como resultado de ésta sesion?',
                'tipo'=>'Abierta',
                'evento'=>4
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende ésta sesión a un cliente o colega?',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted actúe sobre lo concretado?',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este facilitador a otros?',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>4
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó de esta sesión?',
                'tipo'=>'Abierta',
                'evento'=>4
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar ?',
                'tipo'=>'Abierta',
                'evento'=>4
            ),

        );

        $vip_list = array(
            array(
                'pregunta'=>'¿Qué esperaba lograr como resultado de este evento?',
                'tipo'=>'Abierta',
                'evento'=>5
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende ésta sesión a un cliente o colega?',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted actúe sobre lo concretado?',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este facilitador a otros?',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>5
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó de esta evento?',
                'tipo'=>'Abierta',
                'evento'=>5
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar de este evento?',
                'tipo'=>'Abierta',
                'evento'=>5
            ),

        );

        $coach_list = array(
            array(
                'pregunta'=>'¿Qué esperaba lograr con su participación en este proceso de coaching?',
                'tipo'=>'Abierta',
                'evento'=>6
            ),
            array(
                'pregunta'=>'¿Qué tan satisfecho está con el logro de sus expectativas?',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),
            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende ésta sesión a un cliente o colega?',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted aplique lo que aprendió?',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),

            array(
                'pregunta'=>'¿Qué tan probable es que usted recomiende este coach a otros?',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),

            array(
                'pregunta'=>'Califique la calidad de los materiales',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),

            array(
                'pregunta'=>'Califique el ambiente del salón, áreas adyacentes y servicios durante el evento',
                'tipo'=>'Cerrada',
                'evento'=>6
            ),

            array(
                'pregunta'=>'¿Qué es lo que más le gustó del proceso de coaching?',
                'tipo'=>'Abierta',
                'evento'=>6
            ),

            array(
                'pregunta'=>'¿Qué debemos mejorar de coaching?',
                'tipo'=>'Abierta',
                'evento'=>6
            ),

        );


        $orden=1;
        $pre=1;
          foreach ($taller_list as $talle)
          {
              DB::table('preguntas')->insert([
                  'description' => $talle['pregunta'],
                  'orden' => $orden,
                  'tipo'=>$talle['tipo'],
                  'id_tipo_evento'=>$talle['evento'],
                  'created_at' => Carbon::now(),
                  'updated_at' => Carbon::now()
              ]);

              if($talle['tipo']=='Cerrada')
              {
                  for ($con = 1; $con <= 11; $con++)
                  {
                      DB::table('preguntas_respuestas')->insert([
                          'id_preguntas' => $pre,
                          'id_respuestas' => $con,
                          'created_at' => Carbon::now(),
                          'updated_at' => Carbon::now()
                      ]);
                  }
              }

              $orden=$orden+1;
              $pre=$pre+1;

          }

        $orden=1;
        foreach ($proceso_list as $proceso)
        {
            DB::table('preguntas')->insert([
                'description' => $proceso['pregunta'],
                'orden' => $orden,
                'tipo'=>$proceso['tipo'],
                'id_tipo_evento'=>$proceso['evento'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($proceso['tipo']=='Cerrada')
            {
                for ($con = 1; $con <= 11; $con++)
                {
                    DB::table('preguntas_respuestas')->insert([
                        'id_preguntas' => $pre,
                        'id_respuestas' => $con,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
            $pre=$pre+1;
            $orden=$orden+1;
        }

        $orden=1;
        foreach ($aca_list as $aca)
        {
            DB::table('preguntas')->insert([
                'description' => $aca['pregunta'],
                'orden' => $orden,
                'tipo'=>$aca['tipo'],
                'id_tipo_evento'=>$aca['evento'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($aca['tipo']=='Cerrada')
            {
                for ($con = 1; $con <= 11; $con++)
                {
                    DB::table('preguntas_respuestas')->insert([
                        'id_preguntas' => $pre,
                        'id_respuestas' => $con,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
            $pre=$pre+1;
            $orden=$orden+1;
        }

        $orden=1;
        foreach ($sesion_list as $sesion)
        {
            DB::table('preguntas')->insert([
                'description' => $sesion['pregunta'],
                'orden' => $orden,
                'tipo'=>$sesion['tipo'],
                'id_tipo_evento'=>$sesion['evento'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($sesion['tipo']=='Cerrada')
            {
                for ($con = 1; $con <= 11; $con++)
                {
                    DB::table('preguntas_respuestas')->insert([
                        'id_preguntas' => $pre,
                        'id_respuestas' => $con,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
            $pre=$pre+1;
            $orden=$orden+1;
        }

        $orden=1;
        foreach ($vip_list as $vip)
        {
            DB::table('preguntas')->insert([
                'description' => $vip['pregunta'],
                'orden' => $orden,
                'tipo'=>$vip['tipo'],
                'id_tipo_evento'=>$vip['evento'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($vip['tipo']=='Cerrada')
            {
                for ($con = 1; $con <= 11; $con++)
                {
                    DB::table('preguntas_respuestas')->insert([
                        'id_preguntas' => $pre,
                        'id_respuestas' => $con,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
            $pre=$pre+1;
            $orden=$orden+1;
        }

        $orden=1;
        foreach ($coach_list as $coach)
        {
            DB::table('preguntas')->insert([
                'description' => $coach['pregunta'],
                'orden' => $orden,
                'tipo'=>$coach['tipo'],
                'id_tipo_evento'=>$coach['evento'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            if($coach['tipo']=='Cerrada')
            {
                for ($con = 1; $con <= 11; $con++)
                {
                    DB::table('preguntas_respuestas')->insert([
                        'id_preguntas' => $pre,
                        'id_respuestas' => $con,
                        'created_at' => Carbon::now(),
                        'updated_at' => Carbon::now()
                    ]);
                }
            }
            $pre=$pre+1;
            $orden=$orden+1;
        }


    }
}
