<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModulesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('modules')->insert([
            'description' => 'Principal',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Principal',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Users',
            'order' => '2',
            'id_father' =>'1',
            'url' =>'users',
            'messages' => 'Users',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Audit',
            'order' => '3',
            'id_father' =>'1',
            'url' =>'audit',
            'messages' => 'Audit',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Modules',
            'order' => '4',
            'id_father' =>'1',
            'url' =>'modules',
            'messages' => 'Modules',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);



        DB::table('modules')->insert([
            'description' => 'Reports',
            'order' => '1',
            'id_father' =>'0',
            'url' =>'#',
            'messages' => 'Reports',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reports Evento Especifico',
            'order' => '1',
            'id_father' =>'5',
            'url' =>'respecifico',
            'messages' => 'Reporte Evento Especifico ',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Grupos',
            'order' => '5',
            'id_father' =>'1',
            'url' =>'grupos',
            'messages' => 'Grupos',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Pais',
            'order' => '6',
            'id_father' =>'1',
            'url' =>'pais',
            'messages' => 'Pais',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Categorias de Soluciones',
            'order' => '7',
            'id_father' =>'1',
            'url' =>'soluciones',
            'messages' => 'Categorias de Soluciones',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Industrias',
            'order' => '6',
            'id_father' =>'1',
            'url' =>'industrias',
            'messages' => 'Industrias',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Facilitador',
            'order' => '7',
            'id_father' =>'1',
            'url' =>'facilitador',
            'messages' => 'Facilitador',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Eventos',
            'order' => '8',
            'id_father' =>'1',
            'url' =>'eventos',
            'messages' => 'eventos',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Tipo Evento',
            'order' => '9',
            'id_father' =>'1',
            'url' =>'tipoeventos',
            'messages' => 'Tipo Evento',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
        DB::table('modules')->insert([
            'description' => 'Preguntas',
            'order' => '10',
            'id_father' =>'1',
            'url' =>'preguntas',
            'messages' => 'Preguntas',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Reporte Evento Consolidado',
            'order' => '2',
            'id_father' =>'5',
            'url' =>'rconsolidado',
            'messages' => 'Reporte Evento Consolidado ',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Resumen de Periodo',
            'order' => '3',
            'id_father' =>'5',
            'url' =>'rperiodo',
            'messages' => 'Reporte Evento Especifico ',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);

        DB::table('modules')->insert([
            'description' => 'Respuestas',
            'order' => '10',
            'id_father' =>'1',
            'url' =>'respuestas',
            'messages' => 'Prespuestas',
            'status' =>'1',
            'visible' =>'1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
