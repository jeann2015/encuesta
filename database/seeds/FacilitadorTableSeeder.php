<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FacilitadorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $facilitador_list = array(
            array(
                'fname'=>"Gustavo",
                'lname'=>'Vargas',
                'email'=>'',
                'id_pais'=>1
                ),
            array(
                'fname'=>"Francisco",
                'lname'=>'Villalta',
                'email'=>'fvillalta@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Eric",
                'lname'=>'Liljenstolpe',
                'email'=>'ericl@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Roberto",
                'lname'=>'Acosta',
                'email'=>'racosta@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Tomás",
                'lname'=>'Morell',
                'email'=>'tmorell@fcla.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Roberto",
                'lname'=>'Alonso',
                'email'=>'ralonso@fcla.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Elisa",
                'lname'=>'Olaciregui',
                'email'=>'elisao@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Dennis",
                'lname'=>'Salas',
                'email'=>'dsalas@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Otto",
                'lname'=>'Peraza',
                'email'=>'operaza@fc-cr.com',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Gustavo",
                'lname'=>'Vargas',
                'email'=>'',
                'id_pais'=>1
            ),
            array(
                'fname'=>"Facilitador Interno",
                'lname'=>'Facilitador Interno',
                'email'=>'elisao@fc-cr.com',
                'id_pais'=>1
            )

        );

        foreach ($facilitador_list as $facil)
        {

            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Arnulfo",
                'lname'=>'Ábrego',
                'email'=>'aabrego@franklincoveyla.com',
                'id_pais'=>2
            ),
            array(
                'fname'=>"Ovidio",
                'lname'=>'Guillen',
                'email'=>'oguillen@franflincoveyla.com',
                'id_pais'=>2
            ),
            array(
                'fname'=>"Marta",
                'lname'=>'Sánchez-De Sola',
                'email'=>'marta.sanchez@franklincoveyla.com',
                'id_pais'=>2
            ),
            array(
                'fname'=>"José Gabriel",
                'lname'=>'Miralles',
                'email'=>'jmiralles@franklincoveyla.com',
                'id_pais'=>2
            ),
            array(
                'fname'=>"Sofia",
                'lname'=>'Socorro',
                'email'=>'ssocorro@franklincoveyla.com',
                'id_pais'=>2
            ),
            array(
                'fname'=>"Ana Maria",
                'lname'=>'de Hernandez',
                'email'=>'adeleon@franklincoveyla.com',
                'id_pais'=>2
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Emilio",
                'lname'=>'Canales',
                'email'=>'ecanales@fcla.com',
                'id_pais'=>3
            ),
            array(
                'fname'=>"César",
                'lname'=>'Sandino',
                'email'=>'cesar.sandino@enitel.com.ni',
                'id_pais'=>3
            ),
            array(
                'fname'=>"Richard",
                'lname'=>'Montenegro',
                'email'=>'rmontenegro@fcla.com',
                'id_pais'=>3
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Pablo",
                'lname'=>'Luengas',
                'email'=>'pablo.luengas@franklincoveymex.com',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Guillermo",
                'lname'=>'Ganem',
                'email'=>'guillermo.ganem@franklincoveymex.com',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Ricardo",
                'lname'=>'Acevedo',
                'email'=>'ricardo.acevedo@franklincoveymex.com',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Lucia",
                'lname'=>'Alvarez',
                'email'=>'lucia.alvarez@franklincoveymex.com',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Rodrigo",
                'lname'=>'del Val',
                'email'=>'rodrigo.delval@anahuac.mx',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Jose Gabriel",
                'lname'=>'Miralles',
                'email'=>'jmiralles@franklincoveyla.com',
                'id_pais'=>4
            ),
            array(
                'fname'=>"Melida",
                'lname'=>'Quiñones',
                'email'=>'mquinones@franklincovey.mx',
                'id_pais'=>4
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Adriana",
                'lname'=>'Avilés de Arriaza',
                'email'=>'aavilesdearriaza@fcla.com',
                'id_pais'=>5
            ),
            array(
                'fname'=>"Humberto",
                'lname'=>'Arriaza',
                'email'=>'harriaza@fcla.com',
                'id_pais'=>5
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Ingrid",
                'lname'=>'Villela',
                'email'=>'ivillela@fcla.com',
                'id_pais'=>6
            ),
            array(
                'fname'=>"Luis",
                'lname'=>'Aleman',
                'email'=>'laleman@fcla.com',
                'id_pais'=>6
            ),
            array(
                'fname'=>"Juan",
                'lname'=>'Solano',
                'email'=>'jsolano@fcla.com',
                'id_pais'=>6
            ),
            array(
                'fname'=>"Kitty",
                'lname'=>'Kaar',
                'email'=>'Kitty.Kaar@FranklinCovey.com',
                'id_pais'=>6
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Karina",
                'lname'=>'Bonilla de Carbajal',
                'email'=>'kbonilla@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Maya",
                'lname'=>'de Mena',
                'email'=>'marchila@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Mónica",
                'lname'=>'de Zelaya',
                'email'=>'mzelaya@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Gladys",
                'lname'=>'González',
                'email'=>'ggonzalez@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Rodrigo",
                'lname'=>'Pérez',
                'email'=>'rperez@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Ana Lucia",
                'lname'=>'Moreno',
                'email'=>'amoreno@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Michelle",
                'lname'=>'Castro',
                'email'=>'mcastro@fcla.com',
                'id_pais'=>7
            ),
            array(
                'fname'=>"Barbara",
                'lname'=>'Hauser',
                'email'=>'performance@barbarahauser.com',
                'id_pais'=>7
            )

        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        $facilitador_list = array(
            array(
                'fname'=>"Lonnie",
                'lname'=>'Moore',
                'email'=>'lonnie.moore@franklincovey.com',
                'id_pais'=>8
            ),
            array(
                'fname'=>"Philip",
                'lname'=>'Ray',
                'email'=>'pray@franklincoveyla.com',
                'id_pais'=>8
            ),
            array(
                'fname'=>"María Paz",
                'lname'=>'Rioseco',
                'email'=>'mrioseco@franklincoveyla.com',
                'id_pais'=>8
            ),
            array(
                'fname'=>"Mónica",
                'lname'=>'Herrera',
                'email'=>'monicaherrerag@msn.com',
                'id_pais'=>8
            ),
            array(
                'fname'=>"Gustavo",
                'lname'=>'Dougherty',
                'email'=>'gdougherty@franklincoveyla.com',
                'id_pais'=>8
            ),
            array(
                'fname'=>"Juan",
                'lname'=>'Elizalde',
                'email'=>'jelizalde@franklincovey.com.ar',
                'id_pais'=>8
            )
        );

        foreach ($facilitador_list as $facil)
        {
            DB::table('facilitador')->insert([
                'fname' => $facil['fname'],
                'lname' => $facil['lname'],
                'email' => $facil['email'],
                'id_pais' => $facil['id_pais'],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
