<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GruposUsuariosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('grupos_usuarios')->insert([
          'id_user' => '1',
          'id_grupos' => '1',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);


    }
}
