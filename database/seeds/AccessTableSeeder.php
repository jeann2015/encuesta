<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AccessTableSeeder extends Seeder
{
   /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        for ($con = 1; $con <= 17; $con++) {
            DB::table('access')->insert([
                'id_user' => '1',
                'id_module' => $con,
                'views' =>'1',
                'inserts' =>'1',
                'modifys' =>'1',
                'deletes' =>'1',
                'status' =>'1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
