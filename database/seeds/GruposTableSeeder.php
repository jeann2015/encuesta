<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GruposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('grupos')->insert([
          'description' => 'Administradores',
          'created_at' => Carbon::now(),
          'updated_at' => Carbon::now()
      ]);

        DB::table('grupos')->insert([
            'description' => 'Standar',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ]);
    }
}
