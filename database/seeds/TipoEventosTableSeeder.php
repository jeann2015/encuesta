<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TipoEventosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tipos = array(
            "Taller",
            "Proceso",
            "Academico",
            "Sesión de Consultoría",
            "VIP",
            "Coaching"
        );

        foreach ($tipos as $tipo)
        {
            DB::table('tipo_evento')->insert([
                'description' => $tipo,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
