<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class IndustriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $industrias = array(
            "Telecomunicaciones/ Media/ Comunicaciones",
            "Seguros/ Servicios Financieros",
            "Manufacturera",
            "Alimentación",
            "Salud / Farmacéutica",
            "Naval",
            "Automotriz",
            "Bienes Raíces",
            "Industrial",
            "Educación",
            "Consultoría / IT / Servicios de ingeniería",
            "Gobierno",
            "Hotelería / Turismo",
            "Productos al detal",
            "Transporte",
            "Otra"

        );

        foreach ($industrias as $industria)
        {
            DB::table('industrias')->insert([
                'description' => $industria,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

    }
}
