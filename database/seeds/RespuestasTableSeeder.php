<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RespuestasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con=0;$con<=10;$con++)
        {
            if($con<=6)
            {$description='Isatisfecho';}
            if($con>6 && $con<=8)
            {$description='Satisfecho';}
            if($con>=9 && $con<=10)
            {$description='Muy Satisfecho';}

                DB::table('respuestas')->insert([
                    'description' => $description,
                    'valor' => $con,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ]);

        }

    }
}
