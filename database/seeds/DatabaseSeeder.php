<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AccessTableSeeder::class);
        $this->call(ModulesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(GruposTableSeeder::class);
        $this->call(GruposUsuariosTableSeeder::class);
        $this->call(GruposModulosTableSeeder::class);
        $this->call(PaisTableSeeder::class);
        $this->call(FacilitadorTableSeeder::class);
        $this->call(TipoEventosTableSeeder::class);
        $this->call(RespuestasTableSeeder::class);
        $this->call(PreguntasTableSeeder::class);
        $this->call(SolucionesTableSeeder::class);
        $this->call(IndustriasTableSeeder::class);
    }
}
