<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class GruposModulosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($con = 1; $con <= 17; $con++) {
            DB::table('grupos_modules')->insert([
                'id_modules' => $con,
                'id_grupos' => '1',
                'views' => '1',
                'inserts' => '1',
                'modifys' => '1',
                'deletes' => '1',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            DB::table('grupos_modules')->insert([
                'id_modules' => $con,
                'id_grupos' => '2',
                'views' => '0',
                'inserts' => '0',
                'modifys' => '0',
                'deletes' => '0',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

        }


    }

}
