@extends('layouts.headerreports')
@section('content')

    @foreach ($eventos as $evento)
    {!! Form::open(array('url' => 'respecifico/ver/report/graph/')) !!}

    @if(Session::has('message'))
        <p class="alert alert-info">{{ Session::get('message') }}</p>
    @endif
    <div class="content-wrapper">
        <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="main-header">
                <h4>Reporte Especifico</h4>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <tr>
            <td colspan="2">
                <a href="{{ url('respecifico') }}" class="btn btn-default" role="button">Back </a>
            </td>
        </tr>
        <tr class="info">
            <td colspan="2">
                Datos del Evento
            </td>
        </tr>

        <tr>
            <td>{{ $evento->tipo }}:</td>
            <td colspan="2">
                {{ $evento->tipo }}
            </td>
        </tr>
        <tr>
            <td>Facilitador:</td>
            <td colspan="2">
                {{ $evento->facilitador }}
            </td>
        </tr>
        <tr>
            <td>Fecha:</td>
            <td colspan="2">
                {{ $evento->fecha_evento }}
            </td>
        </tr>
        <tr>
            <td>Organización:</td>
            <td colspan="2">
                {{ $evento->organizacion }}
            </td>
        </tr>
        <tr>
            <td>Lugar:</td>
            <td colspan="2">
                {{ $evento->lugar }}
            </td>
        </tr>

        <tr>
            <td>Asistentes:</td>
            <td colspan="2">
                {{ $evento->asistentes }}
            </td>
        </tr>


        <tr>
            <td>Participantes:</td>
            <td colspan="2">
                {{ $evento->participantes }}
            </td>
        </tr>


        <tr>
            <td colspan="2">
                {!! Form::submit('Lanzar!',array('class' => 'btn btn-primary','id'=>'lanzar')) !!}
                {!! Form::hidden('id',$evento->id,array('id'=>'id')) !!}
            </td>
        </tr>

        <tr class="success">
            <td colspan="2">Opciones</td>
        </tr>
        <tr class="success">
            <td colspan="2">
                {!! Form::select('opciones',['1'=>'Ver en Pantalla','2'=>'Enviar por Correo'],'', ['class'=>'form-control','required','onclick'=>'enviar(this.value)']) !!}
            </td>
        </tr>


    </table>
            {!! Form::close() !!}

            {!! Form::open(array('url' => 'respecifico/ver/report/graph/','files'=>true)) !!}

            <div id="enviar_correo" style="visibility: hidden">
                <table class="table">
                    <tr>
                        <td>Asunto: </td>
                        <td>
                            {!! Form::text('asunto','Reporte Especifico',array('class' => 'form-control','id'=>'asunto','required')) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Correo: </td>
                        <td>
                            {!! Form::email('email','',array('class' => 'form-control','id'=>'email','required')) !!}
                        </td>
                    </tr>
                    <tr>
                        <td>Archivo: </td>
                        <td>
                            {!! Form::file('archivo',[],array('class' => 'form-control','id'=>'archivo','required')) !!}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            {!! Form::submit('Enviar!',array('class' => 'btn btn-primary','id'=>'enviar')) !!}
                            {!! Form::hidden('opciones','2',array('class' => 'form-control','id'=>'opciones')) !!}
                            {!! Form::hidden('id',$evento->id,array('class' => 'form-control','id'=>'id')) !!}
                        </td>
                    </tr>

                </table>
            </div>

            {!! Form::close() !!}

        </div>
    </div>

    @endforeach
@endsection