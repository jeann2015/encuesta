@extends('layouts.headerreports')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Reporte Especifico</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'respecifico/search')) !!}
                            <div class="form-group">
                                <label>Pais:</label>
                                {!! Form::select('pais',$pais, $values['pais'], ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo:</label>
                                {!! Form::select('tipoevento', $tipoeventos, $values['tipoeventos'], ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Solución:</label>
                                {!! Form::select('solucion',$soluciones,$values['soluciones'], ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Industrias:</label>
                                {!! Form::select('industria',$industrias, $values['industrias'], ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Facilitador:</label>
                                {!! Form::select('facilitador',$facilitador,$values['facilitador'], ['class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <table class="table">
                                    <tr>
                                        <td>Desde</td>
                                        <td>{!! Form::text('desde', $values['desde'] ,array('class' => 'form-control','id'=>'desde','required')) !!}</td>
                                        <td>Hasta</td>
                                        <td>{!! Form::text('hasta',$values['hasta'],array('class' => 'form-control','id'=>'hasta','required')) !!}</td>
                                    </tr>

                                </table>

                            </div>
                            <div class="form-group">
                                {!! Form::submit('Buscar!',array('class' => 'btn btn-primary','id'=>'buscar')); !!}
                            </div>


                           <table class="table">
                            <tr class="success">
                            <td>Fecha</td>
                            <td>Solución</td>
                            <td>Consultores</td>
                            <td>Pais</td>
                            <td>Organizacion</td>
                            <td>Ver</td>
                            </tr>
                            @isset($eventos)
                            @foreach ($eventos as $evento)
                            <tr>
                            <td>{{
                            Carbon\Carbon::parse($evento->fecha_evento)->format('l jS \\of F Y')
                            }}
                            </td>
                            <td>{{ $evento->solucion }}</td>
                            <td>{{ $evento->facilitador }}</td>
                            <td>{{ $evento->pais }}</td>
                            <td>{{ $evento->organizacion }}</td>
                            <td><a href="{{ url('respecifico/ver/report/'.$evento->id) }}" class="btn btn-success" role="button">Ver</a></td>
                            </tr>
                            @endforeach
                            @endunless
                            </table>

                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


