<html>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Jean Carlos Nunez">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link href="{{ asset('css/ablePro.css') }}" rel="stylesheet">
    <head>
        <?php $base = 'http'.(@$_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/'; echo "<base href=\"{$base}\">"; ?>
        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body class="sidebar-mini fixed">
        <div class="wrapper">

        <header class="main-header-top hidden-print">
            <a href="" class="logo"><img class="img-fluid" src="assets/images/logoFC.png" alt="Theme-logo"></a>
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#!" data-toggle="offcanvas" class="sidebar-toggle"></a>
                <div class="navbar-custom-menu">

                    <ul class="top-nav">
                        <li class="dropdown pc-rheader-submenu message-notification search-toggle" style="display: none;">
                            <a href="#!" id="morphsearch-search" class="drop icon-circle txt-white">
                                <i class="icofont icofont-search-alt-1"></i>
                            </a>
                        </li>
                        <!-- Authentication Links - User Menu -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle drop icon-circle drop-image" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <span>{{ Auth::user()->name }} <i class=" icofont icofont-simple-down"></i></span>
                                </a>

                                <ul class="dropdown-menu settings-menu" role="menu">
                                    <li class="dropdown">
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="icon-logout"></i> Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                     <!-- search -->
                    <div id="morphsearch" class="morphsearch" style="display: none;">
                        <form class="morphsearch-form">

                            <input class="morphsearch-input" type="search" placeholder="Search..."/>

                            <button class="morphsearch-submit" type="submit">Search</button>

                        </form>
                        <div class="morphsearch-content">
                            <div class="dummy-column">
                                <h2>People</h2>
                                <a class="dummy-media-object" href="#!">
                                    <img class="round" src="http://0.gravatar.com/avatar/81b58502541f9445253f30497e53c280?s=50&d=identicon&r=G" alt="Sara Soueidan"/>
                                    <h3>Sara Soueidan</h3>
                                </a>

                                <a class="dummy-media-object" href="#!">
                                    <img class="round" src="http://1.gravatar.com/avatar/9bc7250110c667cd35c0826059b81b75?s=50&d=identicon&r=G" alt="Shaun Dona"/>
                                    <h3>Shaun Dona</h3>
                                </a>
                            </div>
                            <div class="dummy-column">
                                <h2>Popular</h2>
                                <a class="dummy-media-object" href="#!">
                                    <img src="assets/images/avatar-1.png" alt="PagePreloadingEffect"/>
                                    <h3>Page Preloading Effect</h3>
                                </a>

                                <a class="dummy-media-object" href="#!">
                                    <img src="assets/images/avatar-1.png" alt="DraggableDualViewSlideshow"/>
                                    <h3>Draggable Dual-View Slideshow</h3>
                                </a>
                            </div>
                            <div class="dummy-column">
                                <h2>Recent</h2>
                               <a class="dummy-media-object" href="#!">
                                    <img src="assets/images/avatar-1.png" alt="TooltipStylesInspiration"/>
                                    <h3>Tooltip Styles Inspiration</h3>
                                </a>
                                <a class="dummy-media-object" href="#!">
                                    <img src="assets/images/avatar-1.png" alt="NotificationStyles"/>
                                    <h3>Notification Styles Inspiration</h3>
                                </a>
                            </div>
                        </div><!-- /morphsearch-content -->
                        <span class="morphsearch-close"><i class="icofont icofont-search-alt-1"></i></span>
                    </div>
                    <!-- search end -->
                    <!-- <div class="collapse navbar-collapse" id="app-navbar-collapse">

                        <ul class="nav navbar-nav">
                            &nbsp;
                        </ul>

                        <ul class="nav navbar-nav navbar-right">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Login</a></li>
                                <li><a href="{{ route('register') }}">Register</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu" role="menu">
                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div> -->
                </div>
            </nav>
        </header>
        <aside class="main-sidebar hidden-print">
            <section class="sidebar" id="sidebar-scroll">
                <ul class="sidebar-menu">
                    @foreach ($module_principals as $module_principal)
                        <li class="treeview active"><a href="#"><i class="icon-docs"></i><span> {{ $module_principal->description }} </span><i class="icon-arrow-down"></i></a>
                          <ul class="treeview-menu">
                            @foreach ($module_menus as $module_menu)
                                @if ($module_menu->id_father == $module_principal->id && $module_menu->visible==1 && $module_menu->views==1)
                                <li><a href="{{ $module_menu->url }}" class="waves-effect waves-dark"><i class="icon-arrow-right"></i><span>{{ $module_menu->description }}</span></a></li>
                                @endif
                            @endforeach
                          </ul>
                        </li>
                    @endforeach
                </ul>

            </section>
        </aside>
        @yield('content')

        </div>
    </body>

    <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <!-- tether.js -->
    <script src="assets/js/tether.min.js"></script>

    <!-- waves effects.js -->
    <script src="assets/plugins/waves/js/waves.min.js"></script>

    <!-- Custom js -->
    <script type="text/javascript" src="assets/pages/elements.js"></script>

    <!-- Scrollbar JS-->
    <script src="assets/plugins/slimscroll/js/jquery.slimscroll.js"></script>
    <script src="assets/plugins/slimscroll/js/jquery.nicescroll.min.js"></script>

    <!--classic JS-->
    <script src="assets/plugins/search/js/classie.js"></script>

    <!-- table edit js  >
    <script src="assets/plugins/edit-table/js/jquery.tabledit.js"></script -->

    <!-- custom js -->
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/pages/elements.js"></script>
    <script src="assets/js/menu.js"></script>

    <script type="text/javascript">
         $('.treeview.active > a').on("click", function(){ return false; })
    </script>

</html>
