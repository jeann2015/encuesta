<html>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="Jean Carlos Nunez">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <link href="{{ asset('css/ablePro.css') }}" rel="stylesheet">
    <head>
        <?php $base = 'http'.(@$_SERVER['HTTPS'] ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/'; echo "<base href=\"{$base}\">"; ?>
        <title>{{ config('app.name', 'Laravel') }}</title>
    </head>
    <body class="">


        <header class="main-header-top hidden-print">
            <a href="" class="logo"><img class="img-fluid" src="assets/images/logoFC.png" alt="Theme-logo"></a>
        </header>

        @yield('content')


    </body>

    <script src="http://code.jquery.com/jquery-1.12.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.28/vue.min.js"></script>
    <script src="assets/js/jquery-ui.min.js"></script>
    <!-- tether.js -->
    <script src="assets/js/tether.min.js"></script>

    <!-- waves effects.js -->
    <script src="assets/plugins/waves/js/waves.min.js"></script>

    <!-- Custom js -->
    <script type="text/javascript" src="assets/pages/elements.js"></script>

    <!-- Scrollbar JS-->
    <script src="assets/plugins/slimscroll/js/jquery.slimscroll.js"></script>
    <script src="assets/plugins/slimscroll/js/jquery.nicescroll.min.js"></script>

    <!--classic JS-->
    <script src="assets/plugins/search/js/classie.js"></script>

    <!-- table edit js  >
    <script src="assets/plugins/edit-table/js/jquery.tabledit.js"></script -->

    <!-- custom js -->
    <script type="text/javascript" src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/pages/elements.js"></script>
    <script src="assets/js/menu.js"></script>


</html>
