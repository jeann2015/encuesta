@extends('layouts.header')
@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Auditorias</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Usuario</th>
                                    <th>Descripción</th>
                                    <th>Fecha/Hora</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($audits as $audit)
                                    <tr>
                                        <td>{{ $audit->id }}</td>
                                        <td>{{ $audit->name }}</td>
                                        <td>{{ $audit->description }}</td>
                                        <td>{{ Carbon\Carbon::parse($audit->created_at)->format('l jS \\of F Y h:i:s A')  }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $audits->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection