@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Modificar Pais</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('pais') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'pais/update')) !!}
						<div class="form-group">
							<label>Name</label>
							{!! Form::text('description',$pais->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Iniciales</label>
							{!! Form::text('iniciales',$pais->iniciales,array('class' => 'form-control','id'=>'iniciales','required')) !!}
						</div>
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                		{!! Form::hidden('id',$pais->id,array('id'=>'id')) !!}			
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
