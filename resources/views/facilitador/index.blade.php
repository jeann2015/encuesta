@extends('layouts.header')
@section('content')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header">
                    <h4>Facilitadores</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <div class="toolbar">
                            @foreach ($user_access as $user_acces)
                                @if($user_acces->inserts == 1)
                                    <a href="{{ url('facilitador/add') }}" class="btn btn-default" role="button">Add </a>
                                @else
                                    <a href="#" class="btn btn-default" role="button">No Add  </a>
                                @endif
                            @endforeach

                            <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                    </div>
                    <div class="card-block">
                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                                <th>Apellido</th>
                                <th>Correo</th>
                                <th>Pais</th>
                                <th>Acciones</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($facilitador as $facilitado)
                                <tr>
                                    <td>{{ $facilitado->id }}</td>
                                    <td>{{ $facilitado->fname }}</td>
                                    <td>{{ $facilitado->lname }}</td>
                                    <td>{{ $facilitado->email }}</td>
                                    <td>{{ $facilitado->description }}</td>
                                    <td>
                                        <div class="tabledit-toolbar btn-toolbar">
                                            <div class="btn-group btn-group-sm">
                                                @foreach ($user_access as $user_acces)
                                                    @if($user_acces->modifys == 1)
                                                        <a href="facilitador/edit/{{ $facilitado->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                            <span class="icofont icofont-ui-edit"></span>
                                                        </a>
                                                    @else
                                                        <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                    @endif
                                                    @if($user_acces->deletes==1)
                                                        <a href="facilitador/delete/{{ $facilitado->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                            <span class="icofont icofont-ui-delete"></span>
                                                        </a>
                                                    @else
                                                        <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                    @endif
                                                @endforeach
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


{{ $facilitador->links() }}
@endsection
