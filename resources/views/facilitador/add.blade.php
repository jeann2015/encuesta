@extends('layouts.header')

@section('content')

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-header">
                    <!-- Tutulo del Formulario -->
                    <h4>Nueva Facilitador</h4>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <a href="{{ url('facilitador') }}" class="btn btn-default" role="button">Back </a>
                    </div>
                    <div class="card-block">
                        <!-- Formulario -->
                        {!! Form::open(array('url' => 'facilitador/new')) !!}
                        <div class="form-group">
                            <label>Nombre</label>
                            {!! Form::text('fname','',array('class' => 'form-control','id'=>'fname','required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            {!! Form::text('lname','',array('class' => 'form-control','id'=>'fname','required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Correo</label>
                            {!! Form::text('email','',array('class' => 'form-control','id'=>'email','required')) !!}
                        </div>
                        <div class="form-group">
                            <label>Pais</label>
                            {!! Form::select('pais',$pais, '', ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                        </div>
                        {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
