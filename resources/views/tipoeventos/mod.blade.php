@extends('layouts.header')
@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Modificar Tipo de Evento</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('tipoeventos') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                          {!! Form::open(array('url' => 'preguntastipo/add/'.$tipoeventos->id,'method' => 'get' )) !!}
                          <div class="form-group">
                          <button type="submit" class="btn btn-primary " >
                              Agregar Pregunta
                          </button>
                          </div>
                            {!! Form::close() !!}
                            {!! Form::open(array('url' => 'tipoeventos/update')) !!}
                            <div class="form-group">
                                <label>Descripción</label>
                                {!! Form::text('description',$tipoeventos->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>

                            <div class="card-block">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Preguntas</th>
                                        <th>Orden</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($preguntas as $pregunta)
                                        <tr>
                                            <td>{{ $pregunta->id }}</td>
                                            <td>{{ $pregunta->description }}</td>
                                            <td>{{ $pregunta->orden }}</td>
                                            <td>
                                                <div class="tabledit-toolbar btn-toolbar">
                                                    <div class="btn-group btn-group-sm">

                                                                <a href="{{ url('preguntastipo/edit/'.$pregunta->id."/".$tipoeventos->id) }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-edit"></span>
                                                                </a>

                                                                <a href="{{ url('preguntastipo/delete/'.$pregunta->id."/".$tipoeventos->id) }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-delete"></span>
                                                                </a>


                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                            {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                            {!! Form::hidden('id',$tipoeventos->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
