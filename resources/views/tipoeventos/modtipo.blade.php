@extends('layouts.header')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Modificar Pregunta</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('tipoeventos/edit/'.$tipo_eventosid->id) }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            {!! Form::open(array('url' => 'preguntastipo/update')) !!}
                            <div class="form-group">
                                <label>Nombre</label>
                                {!! Form::text('description',$preguntas->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo Evento</label>
                                {!! Form::select('tipo_evento',$tipo_eventos, $preguntas->id_tipo_evento, ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo</label>
                                {!! Form::select('tipo',['Cerrada'=>'Cerrada','Abierta'=>'Abierta'], $preguntas->tipo, ['class'=>'form-control','required','onChange'=>'desabilitar(this.value)']) !!}
                            </div>
                            <div class="form-group">
                                <label>Orden</label>
                                {!! Form::text('orden',$preguntas->orden,array('class' => 'form-control','id'=>'orden','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Respuestas</label>
                                @if($preguntas->tipo=='Abierta')
                                {!! Form::select('respuestas[]',$respuestas,$preguntasrespuestas->all(), ['id'=>'respuestas','class'=>'form-control multiple-select','required','multiple' => true,'disabled']) !!}
                                @else
                                {!! Form::select('respuestas[]',$respuestas,$preguntasrespuestas->all(), ['id'=>'respuestas','class'=>'form-control multiple-select','required','multiple' => true]) !!}
                                @endif

                            </div>
                            {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                            {!! Form::hidden('id',$preguntas->id,array('id'=>'id')) !!}
                            {!! Form::hidden('tipo_evento',$tipo_eventosid->id,array('id'=>'tipo_evento')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


<script>
    function desabilitar(valor)
    {
        if(valor=='Abierta'){
            $( "#respuestas" ).prop( "disabled", true );
        }else{
            $( "#respuestas" ).prop( "disabled", false );
        }
    }
</script>

<script>
    function desabilitar(valor)
    {
        if(valor=='Abierta'){
            $( "#respuestas" ).prop( "disabled", true );
        }else{
            $( "#respuestas" ).prop( "disabled", false );
        }
    }
</script>
