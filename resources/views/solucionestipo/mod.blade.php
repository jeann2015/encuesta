@extends('layouts.header')

@section('content')
       <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Modificar Solución Tipo</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('soluciones/edit/'.$soluciones->id) }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'solucionestipos/update')) !!}
                            <div class="form-group">
                                <label>Name</label>
                                {!! Form::text('description',$solucionestipos->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                            {!! Form::hidden('id_tipo',$solucionestipos->id,array('id'=>'id_tipo')) !!}
                            {!! Form::hidden('id',$soluciones->id,array('id'=>'id')) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
