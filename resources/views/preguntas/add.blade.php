@extends('layouts.header')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Nueva Pregunta</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('preguntas') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'preguntas/new')) !!}
                            <div class="form-group">
                                <label>Nombre</label>
                                {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo Evento</label>
                                {!! Form::select('tipo_evento',$tipo_eventos, '', ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo</label>
                                {!! Form::select('tipo',['Cerrada'=>'Cerrada','Abierta'=>'Abierta'], '', ['class'=>'form-control','required','onChange'=>'desabilitar(this.value)']) !!}
                            </div>
                            <div class="form-group">
                                <label>Orden</label>
                                {!! Form::text('orden','',array('class' => 'form-control','id'=>'orden','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Respuestas</label>
                                {!! Form::select('respuestas[]',$respuestas,'', ['id'=>'respuestas','class'=>'form-control multiple-select','required','multiple' => true]) !!}
                            </div>
                            {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


<script>
function desabilitar(valor)
{
    if(valor=='Abierta'){
        $( "#respuestas" ).prop( "disabled", true );
    }else{
        $( "#respuestas" ).prop( "disabled", false );
    }
}
</script>
