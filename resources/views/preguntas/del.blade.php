{{--@extends('layouts.header')--}}

{{--@section('content')--}}
{{--{!! Form::open(array('url' => 'preguntas/update')) !!}--}}

  {{--<table class="table">--}}
    {{--<tr>--}}
        {{--<td colspan="5">--}}

            {{--Modificar Pregunta--}}

        {{--</td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
        {{--<td colspan="5">--}}

            {{--<a href="{{ url('preguntas') }}" class="btn btn-default" role="button">Back </a>--}}

        {{--</td>--}}
    {{--</tr>--}}
        {{--<tr>--}}
            {{--<td>Descripcion</td>--}}
            {{--<td> {!! Form::text('description',$preguntas->description,array('class' => 'form-control','id'=>'description','required')) !!} </td>--}}
        {{--</tr>--}}
      {{--<tr>--}}
          {{--<td>Tipo Evento</td>--}}
          {{--<td>--}}
              {{--{!! Form::select('tipo_evento',$tipo_eventos, $preguntas->id_tipo_evento, ['placeholder' => 'Select','class'=>'form-control','required']) !!}--}}
          {{--</td>--}}
      {{--</tr>--}}
      {{--<tr>--}}
          {{--<td>Tipo</td>--}}
          {{--<td>--}}
              {{--{!! Form::select('tipo',['Cerrada'=>'Cerrada','Abierta'=>'Abierta'], $preguntas->tipo, ['class'=>'form-control','required','onChange'=>'desabilitar(this.value)']) !!}--}}
          {{--</td>--}}
      {{--</tr>--}}
      {{--<tr>--}}
          {{--<td>Orden</td>--}}
          {{--<td>--}}
              {{--{!! Form::text('orden',$preguntas->orden,array('class' => 'form-control','id'=>'orden','required')) !!}--}}
          {{--</td>--}}
      {{--</tr>--}}
      {{--<tr>--}}
          {{--<td>Respuestas</td>--}}
          {{--@if($preguntas->tipo=='Abierta')--}}
          {{--<td>--}}
              {{--{!! Form::select('respuestas[]',$respuestas,$preguntasrespuestas->all(), ['id'=>'respuestas','class'=>'form-control multiple-select','required','multiple' => true,'disabled']) !!}--}}
          {{--</td>--}}
          {{--@else--}}
          {{--<td>--}}
              {{--{!! Form::select('respuestas[]',$respuestas,$preguntasrespuestas->all(), ['id'=>'respuestas','class'=>'form-control multiple-select','required','multiple' => true]) !!}--}}
          {{--</td>--}}
          {{--@endif--}}
      {{--</tr>--}}
        {{--<tr>--}}
            {{--<td colspan="2">--}}
                {{--{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}--}}
                {{--{!! Form::hidden('id',$preguntas->id,array('id'=>'id')) !!}--}}
            {{--</td>--}}
        {{--</tr>--}}
    {{--</table>--}}
  {{--{!! Form::close() !!}--}}

{{--@endsection--}}

@extends('layouts.header')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Eliminar Pregunta</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('preguntas') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            {!! Form::open(array('url' => 'preguntas/destroy')) !!}
                            <div class="form-group">
                                <label>Nombre</label>
                                {!! Form::text('description',$preguntas->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo Evento</label>
                                {!! Form::select('tipo_evento',$tipo_eventos, $preguntas->id_tipo_evento, ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo</label>
                                {!! Form::select('tipo',['Cerrada'=>'Cerrada','Abierta'=>'Abierta'], $preguntas->tipo, ['class'=>'form-control','required','onChange'=>'desabilitar(this.value)']) !!}
                            </div>
                            <div class="form-group">
                                <label>Orden</label>
                                {!! Form::text('orden',$preguntas->orden,array('class' => 'form-control','id'=>'orden','required')) !!}
                            </div>

                            {!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')); !!}
                            {!! Form::hidden('id',$preguntas->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection


<script>
    function desabilitar(valor)
    {
        if(valor=='Abierta'){
            $( "#respuestas" ).prop( "disabled", true );
        }else{
            $( "#respuestas" ).prop( "disabled", false );
        }
    }
</script>

<script>
    function desabilitar(valor)
    {
        if(valor=='Abierta'){
            $( "#respuestas" ).prop( "disabled", true );
        }else{
            $( "#respuestas" ).prop( "disabled", false );
        }
    }
</script>