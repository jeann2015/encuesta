{{--@extends('layouts.header')--}}
{{--@section('content')--}}

{{--<!-- @if(isset($notifys))--}}

  {{--<div class="alert alert-success" role="alert">--}}
    {{--<button class="close" aria-label="Close" data-dismiss="alert" type="button">--}}
    {{--<span aria-hidden="true">×</span>--}}
    {{--</button>--}}
        {{--@foreach ($notifys as $noti => $valor)--}}
          {{--@if($valor == 1)--}}
            {{--<strong>Well done!</strong> You successfully insert your Data.--}}
          {{--@endif--}}
          {{--@if($valor == 2)--}}
            {{--<strong>Well done!</strong> You successfully modified your Data.--}}
          {{--@endif--}}
          {{--@if($valor == 3)--}}
            {{--<strong>Well done!</strong> You successfully removed your Data,--}}
          {{--@endif--}}
        {{--@endforeach--}}

  {{--</div>--}}

{{--@endif -->--}}

{{--<table class="table table-striped">--}}
{{--<tr>--}}
  {{--<td colspan="9">--}}
    {{--Preguntas--}}
  {{--</td>--}}
{{--</tr>--}}
  {{--<tr>--}}
      {{--<td colspan="9">--}}
          {{--@foreach ($user_access as $user_acces)--}}
              {{--@if($user_acces->inserts == 1)--}}
                {{--<a href="{{ url('preguntas/add') }}" class="btn btn-default" role="button">Add </a>--}}
              {{--@else--}}
                {{--<a href="#" class="btn btn-default" role="button">No Add  </a>--}}
              {{--@endif--}}
          {{--@endforeach--}}

          {{--<a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>--}}

      {{--</td>--}}
  {{--</tr>--}}
      {{--<tr class="success">--}}
          {{--<td>Id</td>--}}
          {{--<td>Descripcion</td>--}}
          {{--<td>Tipo</td>--}}
          {{--<td>Orden</td>--}}
          {{--<td>Tipo Evento</td>--}}
          {{--<td>Modify</td>--}}
          {{--<td>Delete</td>--}}
      {{--</tr>--}}

          {{--@foreach ($preguntas as $pregunta)--}}

              {{--<tr>--}}
                  {{--<td>{{ $pregunta->id }}</td>--}}
                  {{--<td>{{ $pregunta->description }}</td>--}}
                  {{--<td>{{ $pregunta->tipo }}</td>--}}
                  {{--<td>{{ $pregunta->orden }}</td>--}}
                  {{--<td>{{ $pregunta->des_tipo_evento }}</td>--}}
                    {{--@foreach ($user_access as $user_acces)--}}
                      {{--@if($user_acces->modifys == 1)--}}
                        {{--<td><a href="preguntas/edit/{{ $pregunta->id }}" class="btn btn-success" role="button">Modify</a></td>--}}
                      {{--@else--}}
                        {{--<td><a href="#" class="btn btn-default" role="button">No Modify</a></td>--}}
                      {{--@endif--}}
                      {{--@if($user_acces->deletes==1)--}}
                        {{--<td><a href="preguntas/delete/{{ $pregunta->id }}" class="btn btn-danger" role="button">Delete</a></td>--}}
                      {{--@else--}}
                        {{--<td><a href="#" class="btn btn-default" role="button">No Delete</a></td>--}}
                      {{--@endif--}}
                    {{--@endforeach--}}
              {{--</tr>--}}
          {{--@endforeach--}}
  {{--</table>--}}
{{--{{ $preguntas->links() }}--}}
{{--@endsection--}}

@extends('layouts.header')
@section('content')

    <!-- @if(isset($notifys))

        <div class="alert alert-success" role="alert">
          <button class="close" aria-label="Close" data-dismiss="alert" type="button">
          <span aria-hidden="true">×</span>
          </button>
@foreach ($notifys as $noti => $valor)
            @if($valor == 1)
                <strong>Well done!</strong> You successfully insert your Data.
@endif
            @if($valor == 2)
                <strong>Well done!</strong> You successfully modified your Data.
@endif
            @if($valor == 3)
                <strong>Well done!</strong> You successfully removed your Data,
@endif
        @endforeach

                </div>

@endif -->

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <h4>Preguntas</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="toolbar">
                                @foreach ($user_access as $user_acces)
                                    @if($user_acces->inserts == 1)
                                        <a href="{{ url('preguntas/add') }}" class="btn btn-default" role="button">Add </a>
                                    @else
                                        <a href="#" class="btn btn-default" role="button">No Add  </a>
                                    @endif
                                @endforeach

                                <a href="{{ url('home') }}" class="btn btn-default" role="button">Back </a>
                            </div>
                        </div>
                        <div class="card-block">
                            <table class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Descripcion</th>
                                    <th>Tipo</th>
                                    <th>Orden</th>
                                    <th>Tipo Evento</th>
                                    <th>Acciones</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($preguntas as $pregunta)
                                    <tr>
                                        <td>{{ $pregunta->id }}</td>
                                        <td>{{ $pregunta->description }}</td>
                                        <td>{{ $pregunta->tipo }}</td>
                                        <td>{{ $pregunta->orden }}</td>
                                        <td>{{ $pregunta->des_tipo_evento }}</td>
                                        <td>
                                            <div class="tabledit-toolbar btn-toolbar">
                                                <div class="btn-group btn-group-sm">
                                                    @foreach ($user_access as $user_acces)
                                                        @if($user_acces->modifys == 1)
                                                            <a href="preguntas/edit/{{ $pregunta->id }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-edit"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                        @endif
                                                        @if($user_acces->deletes==1)
                                                            <a href="preguntas/delete/{{ $pregunta->id }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                <span class="icofont icofont-ui-delete"></span>
                                                            </a>
                                                        @else
                                                            <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>

                    </div>
                    {{ $preguntas->links() }}
                </div>
            </div>
        </div>

    </div>

@endsection