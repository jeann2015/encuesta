@extends('layouts.header')
@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Eliminar Respuesta</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('respuestas') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'respuestas/destroy')) !!}
                            <div class="form-group">
                                <label>Descripcion</label>
                                {!! Form::text('description',$respuestas->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Valor</label>
                                {!! Form::text('valor',$respuestas->valor,array('class' => 'form-control','id'=>'valor','required')) !!}
                            </div>
                            {!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')) !!}
                            {!! Form::hidden('id',$respuestas->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

