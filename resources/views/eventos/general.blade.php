@extends('layouts.encuesta')
@section('content')

    <div align="center" class="content-wrapper">
        <div class="container-fluid">
                <div align="center" class="col-lg-8">
                    <div class="card">
                        <div class="card-block">
                            {!! Form::open(array('url' => '/buscar/encuesta')) !!}
                            <div class="form-group">
                                <label>Numero de Evento</label>
                                {!! Form::number('id','',array('class' => 'form-control','id'=>'id','required','autofocus')) !!}
                            </div>
                            {!! Form::submit('Buscar!',array('class' => 'btn btn-primary','id'=>'search')); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
        </div>
    </div>

@endsection
