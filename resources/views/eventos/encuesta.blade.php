@extends('layouts.headerencuestas')

@section('content')

    {!! Form::open(array('url' => 'eventos/encuesta')) !!}

    <table class="table">
        <tr class="info">
            <td colspan="5">
                Encuesta
            </td>
        </tr>

        <tr>
            <td>Descripción</td>
            <td> {!! Form::text('description',$eventos->description,array('class' => 'form-control','id'=>'description','required','disabled')) !!} </td>
        </tr>
        <tr>
            <td>Organización</td>
            <td> {!! Form::text('organization',$eventos->organizacion,array('class' => 'form-control','id'=>'organization','required','disabled')) !!} </td>
        </tr>
        <tr>
            <td>Lugar</td>
            <td> {!! Form::text('lugar',$eventos->lugar,array('class' => 'form-control','id'=>'lugar','required','disabled')) !!} </td>
        </tr>
        <tr>
            <td>País</td>
            <td>
                {!! Form::select('pais',$pais, $eventos->id_pais, ['placeholder' => 'Select','class'=>'form-control','required','disabled']) !!}
            </td>
        </tr>

        <tr>
            <td>Tipo de Evento</td>
            <td>
                {!! Form::select('tipoevento', $tipoeventos, $eventos->id_tipo_evento, ['placeholder' => 'Select','class'=>'form-control','required','disabled']) !!}
            </td>
        </tr>
        <tr>
            <td>Solución</td>
            <td>
                {!! Form::select('solucion',$soluciones, $eventos->id_solucion, ['placeholder' => 'Select','class'=>'form-control','required','disabled']) !!}
            </td>
        </tr>
        <tr>
            <td>Industría</td>
            <td>
                {!! Form::select('industria',$industrias, $eventos->id_industrias, ['placeholder' => 'Select','class'=>'form-control','required','disabled']) !!}
            </td>
        </tr>
        <tr>
            <td>Facilitador</td>
            <td>
                {!! Form::select('facilitador',$facilitador,$facilitador->all(), ['class'=>'form-control multiple-select','required','disabled','multiple' => true]) !!}

            </td>
        </tr>
        <tr class="danger">
                    <td colspan="2" align="center">

                    Autoriza usar tus comentarios para uso Interno?
                    {!! Form::checkbox('autorizar',0,false,array('id'=>'autorizar','onclick'=>'changeValue(this.value,"autorizar")')) !!}
                    </td>
        </tr>
        <tr class="info">
            <td colspan="2" align="center">Preguntas</td>

            @foreach($preguntas as $pregunta)
                    <tr>
                    <td colspan="2">
                        {{ $pregunta->description }}
                    </td>
                    </tr>
                    @if ($pregunta->tipo=='Cerrada')
{{--                        @foreach($respuestas as $respuesta)--}}
{{--                            @if($respuesta->id_preguntas == $pregunta->id)--}}
                                <tr>
                                    <td colspan="2">
                                        {!! Form::select('pre'.$pregunta->id,$respuestas->all(),'', ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                                    </td>
                                </tr>
                            {{--@endif--}}
                        {{--@endforeach--}}
                        @else
                        <tr>
                            <td colspan="2">
                                {!! Form::textarea('pre'.$pregunta->id,'',['class' => 'form-control','id'=>'pre'.$pregunta->id,'required','size'=>'5x5']) !!}
                            </td>
                        </tr>
                    @endif
            @endforeach
        </tr>

        <tr>
            <td colspan="5">
                {!! Form::submit('Enviar!',array('class' => 'btn btn-primary','id'=>'send')); !!}
                {!! Form::hidden('id',$eventos->id,array('id'=>'id')) !!}
                {!! Form::hidden('count_pre',$preguntas->count(),array('id'=>'count_pre')) !!}

            </td>
        </tr>
        </tr>
    </table>


    {!! Form::close() !!}

@endsection

<script type="text/javascript" charset="utf-8">

function changeValue(valor,checkbox){

        if(valor==0){
            $('#'+checkbox).val(1);
        }else{
            $('#'+checkbox).val(0);
        }
    }
    
</script>
