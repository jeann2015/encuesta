@extends('layouts.headerencuestas')

@section('content')
    {!! Form::open(array('url' => '/')) !!}

    <table class="table">
        <tr>
            <td colspan="5">

                <div aling="center" class="jumbotron">
                    <p>{{ config('app.name', 'Laravel') }}</p>
                    <h1>Muchas Gracias por Hacer la Encuesta!</h1>

                    <p><a class="btn btn-primary btn-lg" href="{{ url('/') }}" role="button">Presione para ir al Home</a></p>
                </div>

            </td>
        </tr>

    </table>


    {!! Form::close() !!}

@endsection
