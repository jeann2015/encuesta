@extends('layouts.headerevento')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Modificar Evento</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('eventos') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'eventos/update')) !!}
                            <div class="form-group">
                                <label>Nombre</label>
                                {!! Form::text('description',$eventos->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Organizacion</label>
                                {!! Form::text('organization',$eventos->organizacion,array('class' => 'form-control','id'=>'organization','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Lugar</label>
                                {!! Form::text('lugar',$eventos->lugar,array('class' => 'form-control','id'=>'lugar','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Asistente</label>
                                {!! Form::number('asistente',$eventos->asistentes,array('class' => 'form-control','id'=>'asistente','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Participantes</label>
                                {!! Form::number('participantes',$eventos->participantes,array('class' => 'form-control','id'=>'participantes','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Pais</label>
                                {!! Form::select('pais', $pais, $eventos->id_pais, array('class'=>'form-control','id'=>'participantes','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Fecha de Evento</label>
                                {!! Form::text('fecha', $eventos->fecha_evento ,array('class' => 'form-control','id'=>'fecha','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Fecha de Final de Evento</label>
                                {!! Form::text('fecha_final', $eventos->fecha_final ,array('class' => 'form-control','id'=>'fecha_final','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo Evento</label>
                                {!! Form::select('tipoevento', $tipoeventos, $eventos->id_tipo_evento, ['class'=>'form-control','required','onchange'=>'buscar_preguntas(this.value)']) !!}
                            </div>
                            <div class="form-group">
                                <label>Solucion</label>
                                {!! Form::select('solucion',$soluciones, $eventos->id_solucion, ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Industría</label>
                                {!! Form::select('industria',$industrias, $eventos->id_industrias, ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Facilitador</label>
                                {!! Form::select('facilitador[]',$facilitador,$eventosfacilitador->all(), ['class'=>'form-control multiple-select','required','multiple'=>'true']) !!}
                            </div>
                            <div class="form-group">
                                <label>Preguntas</label>
                                {!! Form::select('preguntas[]',$preguntas,$eventospreguntas->all(), ['id'=>'preguntas','class'=>'form-control multiple-select','required','multiple' => true]) !!}
                            </div>
                            <div class="form-group">
                                <label>Url</label>
{{--                                {!! QrCode::size(100)->generate(Request::url()."/encuesta/id?".$eventos->link) !!}--}}
                                {{ Request::url()."/encuesta/".$eventos->id }}
                            </div>

                            {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')) !!}
                            {!! Form::hidden('id',$eventos->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
