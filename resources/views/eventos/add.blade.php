@extends('layouts.headerevento')

@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Nuevo Evento</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('eventos') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'eventos/new')) !!}
                            <div class="form-group">
                                <label>Nombre</label>
                                {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Organizacion</label>
                                {!! Form::text('organization','',array('class' => 'form-control','id'=>'organization','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Lugar</label>
                                {!! Form::text('lugar','',array('class' => 'form-control','id'=>'lugar','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Asistente</label>
                                {!! Form::number('asistente','',array('class' => 'form-control','id'=>'asistente','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Participantes</label>
                                {!! Form::number('participantes','',array('class' => 'form-control','id'=>'participantes','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Pais</label>
                                {!! Form::select('pais', $pais, '', array('class'=>'form-control','id'=>'participantes','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Fecha de Evento</label>
                                {!! Form::text('fecha', Carbon\Carbon::now()->toDateString() ,array('class' => 'form-control','id'=>'fecha','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Fecha de Final de Evento</label>
                                {!! Form::text('fecha_final', Carbon\Carbon::now()->toDateString() ,array('class' => 'form-control','id'=>'fecha_final','required')) !!}
                            </div>
                            <div class="form-group">
                                <label>Tipo Evento</label>
                                {!! Form::select('tipoevento', $tipoeventos, '', ['class'=>'form-control','required','onchange'=>'buscar_preguntas(this.value)']) !!}
                            </div>
                            <div class="form-group">
                                <label>Solucion</label>
                                {!! Form::select('solucion',$soluciones, '', ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Industría</label>
                                {!! Form::select('industria',$industrias, '', ['placeholder' => 'Select','class'=>'form-control','required']) !!}
                            </div>
                            <div class="form-group">
                                <label>Facilitador</label>
                                {!! Form::select('facilitador[]',$facilitador,'', ['class'=>'form-control multiple-select','required','multiple'=>'true']) !!}
                            </div>
                            <div class="form-group">
                                <label>Preguntas</label>
                                {!! Form::select('preguntas[]',$preguntas,'', ['id'=>'preguntas','class'=>'form-control multiple-select','required','multiple' => true]) !!}
                            </div>
                            {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
