@extends('layouts.header')

@section('content')

<div class="content-wrapper">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="main-header">
					<!-- Tutulo del Formulario -->
					<h4>Editar Módulo</h4>
				</div>
			</div>
		</div>	
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-header">
						<a href="{{ url('modules') }}" class="btn btn-default" role="button">Back </a>
					</div>
					<div class="card-block">
						<!-- Formulario -->
						{!! Form::open(array('url' => 'modules/update')) !!}
						<div class="form-group">
							<label>Descripción</label>
							{!! Form::text('description',$modules->description,array('class' => 'form-control','id'=>'description','required')) !!}
						</div>
						<div class="form-group">
							<label>Order</label>
							{!! Form::text('order',$modules->order,array('class' => 'form-control','id'=>'order','required')) !!}
						</div>
						<div class="form-group">
							<label>ID-father</label>
							{!! Form::text('id_father',$modules->id_father,array('class' => 'form-control','id'=>'id_father','required')) !!}
						</div>
						<div class="form-group">
							<label>Messages</label>
							{!! Form::text('messages',$modules->messages,array('class' => 'form-control','id'=>'messages','required')) !!}
						</div>
						<div class="form-group">
							<label>URL</label>
							{!! Form::text('url',$modules->url,array('class' => 'form-control','id'=>'url','required')) !!}
						</div>
						<div class="form-group">
							<label>Visible</label>
							{!! Form::select('visible', array('1' => 'Yes', '0' => 'No'), $modules->visible, ['class'=>'form-control','required']); !!}
						</div>
						<div class="form-group">
							<label>Status</label>
							{!! Form::select('status', array('1' => 'Active', '0' => 'No Active'), $modules->status, ['class'=>'form-control','required']); !!}
						</div>
						{!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                		{!! Form::hidden('id',$modules->id,array('id'=>'id')) !!}
						{!! Form::close() !!}
					</div>
				</div>
			</div>
		</div>
	</div>
</div> 

@endsection