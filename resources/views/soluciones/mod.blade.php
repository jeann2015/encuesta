@extends('layouts.header')
{!! Form::open(array('url' => 'solucionestipos/new')) !!}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Tipo de Solución</h4>
            </div>
            <div class="modal-body">
                Tipo de Solución
                {!! Form::text('descriptiontipo','',array('class' => 'form-control','id'=>'descriptiontipo','required')) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                {!! Form::hidden('id_sol',$soluciones->id,array('id_sol'=>'id')) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Modificar Solución</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('soluciones') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'soluciones/update')) !!}
                            <div class="form-group">
                                <label>Descripción</label>
                                {!! Form::text('description',$soluciones->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            <div class="form-group">
                            <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#myModal">
                                Agregar Tipo de Solución
                            </button>
                            </div>
                            <div class="card-block">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Acciones</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($solucionestipos as $solucionestipo)
                                        <tr>
                                            <td>{{ $solucionestipo->id }}</td>
                                            <td>{{ $solucionestipo->description }}</td>
                                            <td>
                                                <div class="tabledit-toolbar btn-toolbar">
                                                    <div class="btn-group btn-group-sm">
                                                        @foreach ($user_access as $user_acces)
                                                            @if($user_acces->modifys == 1)
                                                                <a href="{{ url( 'solucionestipos/edit/'.$solucionestipo->id."/".$soluciones->id )  }}" class="tabledit-edit-button btn btn-primary waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-edit"></span>
                                                                </a>
                                                            @else
                                                                <a href="#" class="btn btn-default" role="button">No Modify</a>
                                                            @endif
                                                            @if($user_acces->deletes==1)
                                                                <a href="{{ url( 'solucionestipos/delete/'.$solucionestipo->id."/".$soluciones->id ) }}" class="tabledit-delete-button btn btn-danger waves-effect waves-light" role="button">
                                                                    <span class="icofont icofont-ui-delete"></span>
                                                                </a>
                                                            @else
                                                                <a href="#" class="btn btn-default" role="button">No Delete</a>
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>


                            {!! Form::submit('Update!',array('class' => 'btn btn-primary','id'=>'update')); !!}
                            {!! Form::hidden('id',$soluciones->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
