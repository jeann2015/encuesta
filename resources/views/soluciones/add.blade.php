@extends('layouts.header')


{{--<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">--}}
    {{--<div class="modal-dialog" role="document">--}}
        {{--<div class="modal-content">--}}
            {{--<div class="modal-header">--}}
                {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                {{--<h4 class="modal-title" id="myModalLabel">Modal title</h4>--}}
            {{--</div>--}}
            {{--<div class="modal-body">--}}
                {{--...--}}
            {{--</div>--}}
            {{--<div class="modal-footer">--}}
                {{--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>--}}
                {{--<button type="button" class="btn btn-primary">Save changes</button>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}

@section('content')
    {{--{!! Form::open(array('url' => 'soluciones/new')) !!}--}}

    {{--<table class="table">--}}
        {{--<tr>--}}
            {{--<td colspan="5">--}}

                {{--Nueva Solución--}}

            {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td colspan="5">--}}

                {{--<a href="{{ url('soluciones') }}" class="btn btn-default" role="button">Back </a>--}}

            {{--</td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td>Descripción</td>--}}
            {{--<td> {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!} </td>--}}
        {{--</tr>--}}
        {{--<tr>--}}
            {{--<td colspan="2">--}}
                {{--{!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}--}}
            {{--</td>--}}
        {{--</tr>--}}
    {{--</table>--}}
    {{--{!! Form::close() !!}--}}

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Nueva Solución</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('soluciones') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'soluciones/new')) !!}
                            <div class="form-group">
                                <label>Name</label>
                                {!! Form::text('description','',array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>
                            {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
