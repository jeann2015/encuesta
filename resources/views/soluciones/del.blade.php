@extends('layouts.header')
{!! Form::open(array('url' => 'solucionestipos/new')) !!}
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Nueva Tipo de Solución</h4>
            </div>
            <div class="modal-body">
                Tipo de Solución
                {!! Form::text('descriptiontipo','',array('class' => 'form-control','id'=>'descriptiontipo','required')) !!}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                {!! Form::submit('Save!',array('class' => 'btn btn-primary','id'=>'save')); !!}
                {!! Form::hidden('id_sol',$soluciones->id,array('id_sol'=>'id')) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}


@section('content')

    <div class="content-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-header">
                        <!-- Tutulo del Formulario -->
                        <h4>Eliminar Solución</h4>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <a href="{{ url('soluciones') }}" class="btn btn-default" role="button">Back </a>
                        </div>
                        <div class="card-block">
                            <!-- Formulario -->
                            {!! Form::open(array('url' => 'soluciones/destroy')) !!}
                            <div class="form-group">
                                <label>Descripción</label>
                                {!! Form::text('description',$soluciones->description,array('class' => 'form-control','id'=>'description','required')) !!}
                            </div>


                            {!! Form::submit('Delete!',array('class' => 'btn btn-primary','id'=>'delete')); !!}
                            {!! Form::hidden('id',$soluciones->id,array('id'=>'id')) !!}
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
