<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as Collection;

class EventosPreguntasRespuestasAbiertas extends Model
{
    protected $table="eventos_preguntas_respuestas_abiertas";
    protected $fillable = [
        'id_eventos',
        'id_preguntas',
        'respuesta',
        'authorize'
    ];

    public function get_eventos_preguntas_respuestas_graph($id)
    {
        $module_principal = \DB::table('eventos_preguntas_respuestas_abiertas')
            ->select('eventos_preguntas_respuestas_abiertas.respuesta',
                'eventos_preguntas_respuestas_abiertas.id_preguntas')
            ->where('eventos_preguntas_respuestas_abiertas.id_eventos','=',$id)
            ->orderBy('eventos_preguntas_respuestas_abiertas.id')->get();
        return  Collection::make($module_principal);
    }
}
