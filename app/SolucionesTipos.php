<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SolucionesTipos extends Model
{
    protected $table="soluciones_tipos";
    protected $fillable = [
        'id_soluciones',
        'description'
    ];
}
