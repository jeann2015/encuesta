<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as Collection;

class Respuestas extends Model
{
    protected $table="respuestas";
    protected $fillable = [
        'description',
        'valor'
    ];

    public function ListDescriptionValue()
    {

        $data = \DB::table('respuestas')
            ->select(
                'respuestas.id',
                (\DB::raw("CONCAT(respuestas.description,' Valor:',respuestas.valor) as description"))
            )
            ->orderBy('respuestas.id')
            ->get();

        return  Collection::make($data);
    }
}
