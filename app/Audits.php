<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Audits extends Model
{
  protected $table="audits";
  /**
  * @author Jean Carlos Nunez
  *
  */
  public function save_audits($descriptions) {
    $iduser = \Auth::id();
    $audits = new Audits;
    $audits->id_user = $iduser;
    $audits->description = $descriptions;
    $audits->save();
  }
}
