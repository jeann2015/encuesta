<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GruposUsers extends Model
{
    protected $table="grupos_usuarios";

    protected $fillable = [
        'id_user',
        'id_grupos'
    ];
}
