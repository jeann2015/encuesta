<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industrias extends Model
{
    protected $table="industrias";
    protected $fillable = [
          'description'
      ];
}
