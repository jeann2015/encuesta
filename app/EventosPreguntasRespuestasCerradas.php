<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventosPreguntasRespuestasCerradas extends Model
{
    protected $table="eventos_preguntas_respuestas_cerradas";
    protected $fillable = [
        'id_eventos',
        'id_preguntas',
        'id_respuestas'
    ];
}
