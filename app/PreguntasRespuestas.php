<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Collection as Collection;

class PreguntasRespuestas extends Model
{
    protected $table="preguntas_respuestas";
    protected $fillable = [
        'id_preguntas',
        'id_respuestas',
    ];


    public function get_preguntas_respuestas($id)
    {

        $module_principal = \DB::table('preguntas_respuestas')
            ->select('respuestas.id',
                'respuestas.description')
            ->join('respuestas', function($join) use($id){
                $join->on('preguntas_respuestas.id_respuestas', '=', 'respuestas.id');
            })
            ->where('preguntas_respuestas.id_preguntas','=' ,$id)->get()->toArray();

        return  Collection::make($module_principal);

    }




}
