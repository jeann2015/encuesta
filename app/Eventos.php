<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Eventos extends Model
{
    protected $table="eventos";

    protected $fillable = [
        'description',
        'id_user',
        'id_pais',
        'id_industrias',
        'id_solucion',
        'id_tipo_evento',
        'organizacion',
        'lugar',
        'participantes',
        'asistentes',
        'fecha_evento',
        'fecha_final',
        'link'
    ];

    /**
     * @param $id
     * @return mixed
     */
    public function buscar_link($id)
    {
        $select = \DB::table('eventos')
            ->select('eventos.*');
        $select->where('eventos.id', '=', $id);

        $data = $select->orderBy('eventos.id', 'asc')->get();

        return $data;
    }

    /**
     * @param $id
     * @return int
     */
    public function participante($id)
    {
        $eventos = Eventos::find($id);
         Eventos::where('id', $eventos->id)->update(['participantes' => $eventos->participantes+1]);
         return 1;
    }

    /**
     * @param $pais
     * @param $tipo
     * @param $solucion
     * @param $industrias
     * @param $facilitador
     * @param $desde
     * @param $hasta
     * @param $order
     * @return mixed
     */
    public function SearchEvent($pais, $tipo, $solucion, $industrias, $facilitador, $desde, $hasta, $order)
    {
        $data = \DB::table('eventos')
            ->select(
                'eventos.id',
                'eventos.fecha_evento',
                'eventos.organizacion',
                'pais.iniciales as pais',
                'soluciones.description as solucion',
                (\DB::raw("GROUP_CONCAT(facilitador.fname) as facilitador"))
            )
            ->join('soluciones', function ($join) use ($solucion) {
                if ($solucion==0) {
                    $join->on('eventos.id_solucion', '=', 'soluciones.id');
                } else {
                    $join->on('eventos.id_solucion', '=', 'soluciones.id')
                    ->where('eventos.id_solucion', '=', $solucion);
                }
            })
            ->join('eventos_facilitador', function ($join) {
                $join->on('eventos.id', '=', 'eventos_facilitador.id_eventos');
            })
            ->join('facilitador', function ($join) use ($facilitador) {
                if ($facilitador==0) {
                    $join->on('eventos_facilitador.id_facilitador', '=', 'facilitador.id');
                } else {
                    $join->on('eventos_facilitador.id_facilitador', '=', 'facilitador.id')
                    ->where('eventos_facilitador.id_facilitador', '=', $facilitador);
                }
            })
            ->join('industrias', function ($join) use ($industrias) {
                if ($industrias==0) {
                    $join->on('eventos.id_industrias', '=', 'industrias.id');
                } else {
                    $join->on('eventos.id_industrias', '=', 'industrias.id')
                    ->where('eventos.id_industrias', '=', $industrias);
                }
            })
            ->join('pais', function ($join) use ($pais) {
                if ($pais==0) {
                    $join->on('eventos.id_pais', '=', 'pais.id');
                } else {
                    $join->on('eventos.id_pais', '=', 'pais.id')
                    ->where('eventos.id_pais', '=', $pais);
                }
            })
            ->whereBetween('fecha_evento', [$desde,$hasta])
            ->groupBy('eventos.id',
                'eventos.fecha_evento',
                'eventos.organizacion',
                'pais.iniciales',
                'soluciones.description')
            ->get();

        return  $data;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function SearchEventId($id)
    {
        $data = \DB::table('eventos')
            ->select(
                'eventos.id',
                'eventos.fecha_evento',
                'eventos.organizacion',
                'eventos.lugar',
                'eventos.asistentes',
                'eventos.participantes',
                'tipo_evento.description as tipo',
                (\DB::raw("GROUP_CONCAT(facilitador.fname) as facilitador"))
            )
            ->join('soluciones', function ($join) {
                    $join->on('eventos.id_solucion', '=', 'soluciones.id');
            })
            ->join('tipo_evento', function ($join)  {
                    $join->on('eventos.id_tipo_evento', '=', 'tipo_evento.id');
            })
            ->join('eventos_facilitador', function ($join) {
                $join->on('eventos.id', '=', 'eventos_facilitador.id_eventos');
            })
            ->join('facilitador', function ($join) {
                $join->on('eventos_facilitador.id_facilitador', '=', 'facilitador.id');
            })
            ->join('industrias', function ($join)  {
                $join->on('eventos.id_industrias', '=', 'industrias.id');
            })
            ->join('pais', function ($join) {
                $join->on('eventos.id_pais', '=', 'pais.id');
            })
            ->where('eventos.id','=', $id)
            ->groupBy('eventos.id',
                'eventos.fecha_evento',
                'eventos.organizacion',
                'eventos.lugar',
                'pais.iniciales',
                'eventos.asistentes',
                'eventos.participantes',
                'tipo_evento.description',
                'soluciones.description')
            ->get();

        return  $data;
    }

    /**
     * @param $total
     * @param $valor
     * @return string
     */
    private function Promedio($total,$valor)
    {
        return number_format(($valor*100)/$total,2);
    }

    /**
     * @param $id
     * @return array
     */
    public function EventPromedioResumenCerradas($id)
    {
        $dataValues = $this->RespuestasCerradas($id);

        $con=0;
        $v1=0;
        $v2=0;
        $v3=0;

        foreach ($dataValues as $dataValue)
        {
            if($dataValue->valor>=0 && $dataValue->valor<=6)
            {
                $v1=$v1+1;
            }
            if($dataValue->valor>=7 && $dataValue->valor<=8)
            {
                $v2=$v2+1;
            }
            if($dataValue->valor>=9 && $dataValue->valor<=10)
            {
                $v3=$v3+1;
            }
            $con=$con+1;
        }

        $isatisfecha = $this->Promedio($con,$v1);
        $satisfecha = $this->Promedio($con,$v2);
        $muysatisfecha = $this->Promedio($con,$v3);

        $arrays =array(
            'labels'=> [
                "Isatisfecho ".$isatisfecha."% 0-6",
                "Satisfecho ".$satisfecha."% 7-8",
                "Muy Satisfecho ".$muysatisfecha."% 9-10"
            ],
            'datasets'=>[
                [
                'data'=>[
                    $isatisfecha,
                    $satisfecha,
                    $muysatisfecha
                ],
                'backgroundColor'=> [
                        "#B5B6B3",
                        "#3D6987",
                        "#8E9300"
                    ],
                'hoverBackgroundColor'=> [
                        "#B5B6B3",
                        "#3D6987",
                        "#8E9300"
                    ]
            ]
            ]
        );
        return  $arrays;
    }

    /**
     * @param $id
     * @return array
     */
    public function GetParticipantes($id)
    {
        $particpantes = \DB::table('eventos')
            ->select(
                'eventos.participantes',
                'eventos.asistentes'
            )
            ->where('eventos.id','=', $id)
            ->get();

        foreach ($particpantes as $particpante)
        {
            $resultado_resp = number_format(($particpante->participantes *100)/$particpante->asistentes,2);
        }

        $resultado_nresp = number_format(100-$resultado_resp,2);

        $arrays =array(
            'labels'=> [
                "Respondio ".$resultado_resp."%",
                "No Respondio ".$resultado_nresp."%",
            ],
            'datasets'=>[
                [
                    'data'=>[
                        $resultado_resp,
                        $resultado_nresp,
                    ],
                    'backgroundColor'=> [
                        "#B5B6B3",
                        "#8E9300"
                    ],
                    'hoverBackgroundColor'=> [
                        "#B5B6B3",
                        "#8E9300"
                    ]
                ]
            ]
        );
        return $arrays;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function RespuestasCerradas($id)
    {
        $dataValues = \DB::table('eventos_preguntas_respuestas_cerradas')
            ->select(
                'respuestas.valor'
            )
            ->join('respuestas', function ($join) {
                $join->on('eventos_preguntas_respuestas_cerradas.id_respuestas', '=', 'respuestas.id');
            })
            ->where('eventos_preguntas_respuestas_cerradas.id_eventos','=', $id)
            ->get();

        return $dataValues;

    }

    /**
     * @param $id_eventos
     * @param $id_preguntas
     * @return mixed
     */
    private function PreguntasRespuestasCerradas($id_eventos,$id_preguntas)
    {
        $dataValues = \DB::table('eventos_preguntas_respuestas_cerradas')
            ->select(
                'respuestas.valor'
            )
            ->join('respuestas', function ($join) {
                $join->on('eventos_preguntas_respuestas_cerradas.id_respuestas', '=', 'respuestas.id');
            })
            ->where('eventos_preguntas_respuestas_cerradas.id_eventos','=', $id_eventos)
            ->where('eventos_preguntas_respuestas_cerradas.id_preguntas','=', $id_preguntas)
            ->get();

        return $dataValues;

    }

    /**
     * @param $id_eventos
     * @param $id_preguntas
     * @param $id_respuestas
     * @return mixed
     */
    private function PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,$id_respuestas)
    {
        $dataValues = \DB::table('eventos_preguntas_respuestas_cerradas')
            ->select(
                (\DB::raw("count(*) as valor"))
            )
            ->where('eventos_preguntas_respuestas_cerradas.id_eventos','=', $id_eventos)
            ->where('eventos_preguntas_respuestas_cerradas.id_preguntas','=', $id_preguntas)
            ->where('eventos_preguntas_respuestas_cerradas.id_respuestas','=', $id_respuestas)
            ->get();

        return $dataValues;

    }

    /**
     * @param $id_eventos
     * @param $id_preguntas
     * @return array
     */
    private function GetDatosEvenetosPreguntasRespuestasBarras($id_eventos,$id_preguntas)
    {

        $v0 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,1)->pluck('valor');

        if($v0[0]>0){
            $label[] = "0-Isatisfecho(".$v0[0].")";
            $v[]=$v0[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v1 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,2)->pluck('valor');

        if($v1[0]>0){
            $label[] = "1-Isatisfecho(".$v1[0].")";
            $v[]=$v1[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v2 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,3)->pluck('valor');

        if($v2[0]>0){
            $label[] = "2-Isatisfecho(".$v2[0].")";
            $v[]=$v2[0];
            $color[]="rgb(181, 182, 179)";
        }


        $v3 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,4)->pluck('valor');

        if($v3[0]>0){
            $label[] = "3-Isatisfecho(".$v3[0].")";
            $v[]=$v3[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v4 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,5)->pluck('valor');

        if($v4[0]>0){
            $label[] = "4-Isatisfecho(".$v4[0].")";
            $v[]=$v4[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v5 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,6)->pluck('valor');

        if($v5[0]>0){
            $label[] = "5-Isatisfecho(".$v5[0].")";
            $v[]=$v5[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v6 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,7)->pluck('valor');

        if($v6[0]>0){
            $label[] = "6-Isatisfecho(".$v6[0].")";
            $v[]=$v6[0];
            $color[]="rgb(181, 182, 179)";
        }

        $v7 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,8)->pluck('valor');

        if($v7[0]>0){
            $label[] = "7-Satisfecho(".$v7[0].")";
            $v[]=$v7[0];
            $color[]="rgb(61, 105, 135)";
        }

        $v8 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,9)->pluck('valor');

        if($v8[0]>0){
            $label[] = "8-Satisfecho(".$v8[0].")";
            $v[]=$v8[0];
            $color[]="rgb(61, 105, 135)";
        }


        $v9 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,10)->pluck('valor');

        if($v9[0]>0){
            $label[] = "9-Muy Satisfecho(".$v9[0].")";
            $v[]=$v9[0];
            $color[]="rgb(142, 147, 0)";
        }

        $v10 = $this->PreguntasRespuestasCerradasValor($id_eventos,$id_preguntas,11)->pluck('valor');

        if($v10[0]>0){
            $label[] = "10-Muy Satisfecho(".$v10[0].")";
            $v[]=$v10[0];
            $color[]="rgb(142, 147, 0)";
        }



        $data = array(
            'labels'=> $label,
            'datasets'=> [
                [
                    'label' => "Promedio" ,
                    'backgroundColor' => $color,
                    'borderColor' => $color,
                    'borderWidth'=> 1,
                    'data'=> $v,
                ]
            ]
            );
        return $data;
    }

    public function GetDatosEventosPreguntasRespuestas($id_eventos,$id_preguntas)
    {
        $dataValues = $this->PreguntasRespuestasCerradas($id_eventos,$id_preguntas);

        $con=0;
        $v1=0;
        $v2=0;
        $v3=0;

        foreach ($dataValues as $dataValue)
        {
            if($dataValue->valor>=0 && $dataValue->valor<=6)
            {
                $v1=$v1+1;
            }
            if($dataValue->valor>=7 && $dataValue->valor<=8)
            {
                $v2=$v2+1;
            }
            if($dataValue->valor>=9 && $dataValue->valor<=10)
            {
                $v3=$v3+1;
            }
            $con=$con+1;
        }

        $isatisfecha = $this->Promedio($con,$v1);
        $satisfecha = $this->Promedio($con,$v2);
        $muysatisfecha = $this->Promedio($con,$v3);

        $arrays =array(
            'labels'=> [
                "Isatisfecho ".$isatisfecha."% 0-6",
                "Satisfecho ".$satisfecha."% 7-8",
                "Muy Satisfecho ".$muysatisfecha."% 9-10"
            ],
            'datasets'=>[
                [
                    'data'=>[
                        $isatisfecha,
                        $satisfecha,
                        $muysatisfecha
                    ],
                    'backgroundColor'=> [
                        "#B5B6B3",
                        "#3D6987",
                        "#8E9300"
                    ],
                    'hoverBackgroundColor'=> [
                        "#B5B6B3",
                        "#3D6987",
                        "#8E9300"
                    ]
                ]
            ]
        );

        return  $arrays;
    }

    public function GetDatosEventosPreguntas($id)
    {
        $dataValues = \DB::table('eventos_preguntas')
            ->select(
                'eventos_preguntas.id_preguntas'
            )
            ->join('preguntas', function ($join) {
                $join->on('eventos_preguntas.id_preguntas', '=', 'preguntas.id')
                ->where('preguntas.tipo','=','Cerrada');
            })
            ->where('eventos_preguntas.id_eventos','=', $id)
            ->get();


        foreach ($dataValues as $dataValue)
        {
            $arrays[$dataValue->id_preguntas]['donut']=$this->GetDatosEventosPreguntasRespuestas($id,$dataValue->id_preguntas);
            $arrays[$dataValue->id_preguntas]['barra']=$this->GetDatosEvenetosPreguntasRespuestasBarras($id,$dataValue->id_preguntas);

        }

        return $arrays;

    }

    public function GetPromedioNeto($id)
    {

        $dataValues = $this->RespuestasCerradas($id);

        $con=0;
        $v1=0;
        $v2=0;
        $v3=0;
        $valor=0;

        foreach ($dataValues as $dataValue)
        {
            if($dataValue->valor>=0 && $dataValue->valor<=6)
            {
                $v1=$v1+1;
                $valor = $valor+$dataValue->valor;
            }
            if($dataValue->valor>=7 && $dataValue->valor<=8)
            {
                $v2=$v2+1;
                $valor = $valor+$dataValue->valor;
            }
            if($dataValue->valor>=9 && $dataValue->valor<=10)
            {
                $v3=$v3+1;
                $valor = $valor+$dataValue->valor;
            }
            $con=$con+1;
        }

        $isatisfecha = $this->Promedio($con,$v1);
        $satisfecha = $this->Promedio($con,$v2);
        $muysatisfecha = $this->Promedio($con,$v3);

        $neto =  number_format($muysatisfecha -  $isatisfecha,2);
        $promedio_general = number_format(($valor)/10,2);


        $arrays =array(
            'promedios_neto'=> $neto,
            'promedios'=> $promedio_general
        );

        return $arrays;
    }
}
