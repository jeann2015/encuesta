<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Industrias;
use App\Modules;

class IndustriasController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
      $module = new Modules;
      $iduser = \Auth::id();

      $module_principals = $module->get_modules_principal_user($iduser);
      $module_menus = $module->get_modules_menu_user($iduser);

      $url = $request->path();
      $user_access = $module->accesos($iduser, $url);

      $industrias = \DB::table('industrias')
          ->select('*')
          ->orderBy('id', 'asc')
          ->paginate(10);

      return view('industrias.index', compact('industrias', 'user_access',
          'module_principals','module_menus'));
  }

  public function add(Request $request)
  {
      $module = new Modules;
      $iduser = \Auth::id();

      $module_principals = $module->get_modules_principal_user($iduser);
      $module_menus = $module->get_modules_menu_user($iduser);

      return view('industrias.add',compact('module_principals','module_menus'));
  }

  public function news(Request $request)
  {
      $audits = new Audits;
      $industrias = Industrias::create(['description'=>$request->description]);
      $audits->save_audits('Add new Industrias:'.$industrias->id." - ".$request->name);

      return redirect('industrias');
  }

  public function edit(Request $request)
  {
      $module = new Modules;
      $iduser = \Auth::id();
      $module_principals = $module->get_modules_principal_user($iduser);
      $module_menus = $module->get_modules_menu_user($iduser);

      $industrias = Industrias::find($request->id);
      return view('industrias.mod', compact('industrias','module_principals','module_menus'));
  }

  public function update(Request $request)
  {
      $audits = new Audits;
      $industrias = Industrias::find($request->id);
      $industrias->description = $request->description;
      $industrias->save();
      $audits->save_audits('Modify a Industrias:'.$request->id." - ".$request->description);
      return redirect('industrias');
  }

  public function delete(Request $request)
  {
      $module = new Modules;
      $iduser = \Auth::id();
      $module_principals = $module->get_modules_principal_user($iduser);
      $module_menus = $module->get_modules_menu_user($iduser);

      $industrias = Industrias::find($request->id);
      return view('industrias.del', compact('industrias','module_principals','module_menus'));
  }

  public function destroy(Request $request)
  {
      $audits = new Audits;

      Industrias::find($request->id)->delete();

      $audits->save_audits('Delete a Industrias:'.$request->id." - ".$request->name);

      return redirect('industrias');
  }
}
