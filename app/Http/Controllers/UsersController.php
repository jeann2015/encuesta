<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Modules;
use App\Access;
use App\Audits;
use App\Grupos;
use App\GruposUsers;
use App\GruposModules;

use Illuminate\Support\Collection as Collection;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {

        $user = new User;
        $iduser = \Auth::id();
        $url = $request->path();
        $module = new Modules;
        $user_access = $module->accesos($iduser,$url);
        $users = User::all();

        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('usuarios.index',compact('users','user_access','module_principals','module_menus'));
    }

    public function add(Request $request){

        $iduser = \Auth::id();
        $grupos = \DB::table('grupos')->pluck('description', 'id');
        $module = new Modules;

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('usuarios.add',compact('grupos','module_principals','module_menus'));

    }

    public function news(Request $request)
    {
        $users = new User;
        $access = new Access;
        $audits = new Audits;

        $modules = Modules::all();

        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = bcrypt($request->passsword);
        $users->save();

        $count_module = $request->count_mod;


        $GruposUsers = new GruposUsers;
        $GruposUsers->id_user = $users->id;
        $GruposUsers->id_grupos = $request->grupos;
        $GruposUsers->save();

        foreach ($modules as $module)
        {
            $access = new Access;
            $gruposmodu = new GruposModules;

            $gruposmodule = $gruposmodu->grupos_modulos($request->grupos,$module->id);

            $gruposmodules = Collection::make($gruposmodule);
            foreach ($gruposmodules as $gruposmodule)
            {
                $view='view'.$module->id;
                $view_value = $gruposmodule->views;
                if($view_value==""){$view_value="0";}

                $save='save'.$module->id;
                $save_value = $gruposmodule->inserts;
                if($save_value==""){$save_value="0";}

                $modify='modify'.$module->id;
                $modify_value = $gruposmodule->modifys;
                if($modify_value==""){$modify_value="0";}

                $delete='delete'.$module->id;
                $delete_value = $gruposmodule->deletes;
                if($delete_value==""){$delete_value="0";}

                if($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"")
                {
                    $access->id_user=$users->id;
                    $access->id_module=$module->id;
                    $access->views=$view_value;
                    $access->inserts=$save_value;
                    $access->modifys=$modify_value;
                    $access->deletes=$delete_value;
                    $access->status=0;
                    $access->save();
                }
            }
        }

        $audits->save_audits('Add new User:'.$users->id." - ".$request->name);
        return redirect('users');

    }

    public function edit(Request $request)
    {

        $iduser = \Auth::id();
        $iduserbuscar = $request->id;
        $users = User::find($iduserbuscar);

        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $gruposusers = \DB::table('grupos_usuarios')->where('id_user','=',$iduserbuscar)->get();

        $gruposuser = Collection::make($gruposusers);


        $grupos = \DB::table('grupos')->pluck('description', 'id');

        $module = \DB::table('modules')
            ->select('modules.id',
                'modules.description',
                'modules.order',
                'modules.id_father',
                'modules.url',
                'modules.messages',
                'modules.status',
                'modules.visible',
                'access.views',
                'access.inserts',
                'access.modifys',
                'access.deletes')
            ->leftjoin('access', function($join) use($iduserbuscar){
                $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduserbuscar);
            })->get();

        $modules = Collection::make($module);

        foreach ($modules as $module)
        {

            if($module->views == 1){
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
            }else{
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
            }

            if($module->inserts == 1){
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
            }else{
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
            }

            if($module->modifys == 1){
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
            }else{
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
            }

            if($module->deletes == 1){
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
            }else{
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
            }


        }

        return view('usuarios.mod',compact('modules','users','arr_view_arr','arr_save_arr','arr_modify_arr','arr_delete_arr','grupos','gruposuser','module_principals','module_menus'));

    }

    public function update(Request $request)
    {
        $audits = new Audits;
        $users = User::find($request->id);
        $users->name = $request->name;
        $users->email = $request->email;
        if($request->password<>"") {
            $users->password = bcrypt($request->password);
        }
        $users->save();

        $modules = Modules::all();

        $grupos_usuarios = \DB::update("update grupos_usuarios set id_grupos = ".$request->grupos."
        where id_user = ".$request->id);

        foreach ($modules as $module)
        {
            $view='view'.$module->id;
            $view_value = $request->$view;
            if($view_value==""){$view_value="0";}

            $save='save'.$module->id;
            $save_value = $request->$save;
            if($save_value==""){$save_value="0";}

            $modify='modify'.$module->id;
            $modify_value = $request->$modify;
            if($modify_value==""){$modify_value="0";}

            $delete='delete'.$module->id;
            $delete_value = $request->$delete;
            if($delete_value==""){$delete_value="0";}

            $access_module = \DB::table('access')
                ->where('access.id_module', '=',$module->id)
                ->where('access.id_user', '=',$request->id)->count();

            if($access_module>0)
            {
                if($view_value<>"" || $save_value<>"" || $modify_value<>"" || $delete_value<>"")
                {
                    $var_datetime = date('Y-m-d H:s:i');
                    $access = \DB::update("update access set views =".$view_value.",inserts=".$save_value.",modifys=".$modify_value.",
  	                deletes=".$delete_value.",updated_at = '".$var_datetime."'
  	                where id_module = ".$module->id." and id_user = ".$request->id);
                }
            }
            else
            {
                $access = new Access;
                $access->id_user=$request->id;
                $access->id_module=$module->id;
                $access->views=0;
                $access->inserts=0;
                $access->modifys=0;
                $access->deletes=0;
                $access->status=0;
                $access->save();
            }

        }

        $audits->save_audits('Modify User:'.$request->id." - ".$request->name);
        return redirect('users');

    }

    public function delete(Request $request)
    {
        $iduser = \Auth::id();
        $iduserbuscar = $request->id;
        $users = User::find($iduserbuscar);

        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $module = \DB::table('modules')
            ->select('modules.id',
                'modules.description',
                'modules.order',
                'modules.id_father',
                'modules.url',
                'modules.messages',
                'modules.status',
                'modules.visible',
                'access.views',
                'access.inserts',
                'access.modifys',
                'access.deletes')
            ->leftjoin('access', function($join) use($iduserbuscar){
                $join->on('access.id_module', '=', 'modules.id')->where('access.id_user', '=',$iduserbuscar);
            })->get();

        $modules = Collection::make($module);

        foreach ($modules as $module)
        {

            if($module->views == 1){
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'1');
            }else{
                $arr_view_arr[$module->id]=array('idview'.$module->id=>'view'.$module->id,'checkedview'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'view'.$module->id."'".')','valueView'.$module->id=>'0');
            }

            if($module->inserts == 1){
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'1');
            }else{
                $arr_save_arr[$module->id]=array('idsave'.$module->id=>'save'.$module->id,'checkedsave'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'save'.$module->id."'".')','valueSave'.$module->id=>'0');
            }

            if($module->modifys == 1){
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'1');
            }else{
                $arr_modify_arr[$module->id]=array('idmodify'.$module->id=>'modify'.$module->id,'checkedmodify'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'modify'.$module->id."'".')','valueModify'.$module->id=>'0');
            }

            if($module->deletes == 1){
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'checked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'1');
            }else{
                $arr_delete_arr[$module->id]=array('iddelete'.$module->id=>'delete'.$module->id,'checkeddelete'.$module->id=>'nochecked','onclick'.$module->id=>'changeValue(this.value,'."'".'delete'.$module->id."'".')','valueDelete'.$module->id=>'0');
            }


        }

        return view('usuarios.del',compact('modules','users','arr_view_arr','arr_save_arr','arr_modify_arr','arr_delete_arr','module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;
        $iduserbuscar = $request->id;
        Access::where('id_user',$iduserbuscar)->delete();
        GruposUsers::where('id_user',$iduserbuscar)->delete();
        $users = User::find($request->id);
        $audits->save_audits('Delete User:'.$request->id." - ".$request->name);
        $users->delete();
        return redirect('users');
    }
}
