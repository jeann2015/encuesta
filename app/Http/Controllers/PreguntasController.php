<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Preguntas;
use App\PreguntasRespuestas;
use App\Modules;
use App\TipoEventos;
use App\Respuestas;

class PreguntasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);


        $preguntas = \DB::table('preguntas')
            ->select('tipo_evento.description as des_tipo_evento','preguntas.orden',
                'preguntas.tipo','preguntas.id','preguntas.description')
            ->orderBy('id', 'asc')
            ->join('tipo_evento','preguntas.id_tipo_evento','=','tipo_evento.id')
            ->paginate(10);
        return view('preguntas.index', compact('preguntas',
            'user_access','user_access','module_principals','module_menus'));
    }

    public function add(Request $request)
    {
        $resp = new Respuestas;

        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $respuestas = $resp->ListDescriptionValue()->pluck('description','id');
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');
        return view('preguntas.add',compact('respuestas',
            'tipo_eventos','user_access','module_principals','module_menus'));
    }

    public function addtipo(Request $request)
    {
        $resp = new Respuestas;

        $module = new Modules;
        $iduser = \Auth::id();


        $respuestas = $resp->ListDescriptionValue()->pluck('description','id');
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $tipo_eventosid = TipoEventos::find($request->idtipoevento);
        return view('tipoeventos.addtipo',compact('respuestas',
            'tipo_eventos','tipo_eventosid',
          'module_principals','module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;

        $preguntas = Preguntas::create([
            'description'=>$request->description,
            'tipo'=>$request->tipo,
            'id_tipo_evento'=>$request->tipo_evento,
            'orden'=>$request->orden
        ]);

        if($request->tipo=='Cerrada')
        {
            foreach ($request->respuestas as $respuesta) {
                PreguntasRespuestas::create([
                    'id_preguntas' => $preguntas->id,
                    'id_respuestas' => $respuesta
                ]);
            }
        }

        $audits->save_audits('Add new Preguntas:'.$preguntas->id." - ".$request->name);
        return redirect('preguntas');
    }

    public function newstipo(Request $request)
    {
        $audits = new Audits;

        $preguntas = Preguntas::create([
            'description'=>$request->description,
            'tipo'=>$request->tipo,
            'id_tipo_evento'=>$request->tipo_evento,
            'orden'=>$request->orden
        ]);

        if($request->tipo=='Cerrada')
        {
            foreach ($request->respuestas as $respuesta) {
                PreguntasRespuestas::create([
                    'id_preguntas' => $preguntas->id,
                    'id_respuestas' => $respuesta
                ]);
            }
        }

        $audits->save_audits('Add new Preguntas:'.$preguntas->id." - ".$request->description);
        return redirect()->action(
            'TipoEventosController@edit', ['id' => $request->tipo_evento]
        );

    }

    public function edit(Request $request)
    {
        $resp = new Respuestas;
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $preguntas = Preguntas::find($request->id);
        $respuestas = $resp->ListDescriptionValue()->pluck('description','id');
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');

        $preguntasrespuestas = \DB::table('preguntas_respuestas')
            ->where('id_preguntas', '=', $request->id)->pluck('id_respuestas');

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('preguntas.mod',
            compact('preguntas','preguntasrespuestas','respuestas',
                'tipo_eventos','module_principals','module_menus'));
    }

    public function edittipo(Request $request)
    {
        $resp = new Respuestas;

        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $preguntas = Preguntas::find($request->id);
        $respuestas = $resp->ListDescriptionValue()->pluck('description','id');
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $tipo_eventosid = TipoEventos::find($request->idtipoevento);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $preguntasrespuestas = \DB::table('preguntas_respuestas')
            ->where('id_preguntas', '=', $request->id)->pluck('id_respuestas');

        return view('tipoeventos.modtipo', compact(
            'preguntas','preguntasrespuestas','respuestas',
            'tipo_eventos','tipo_eventosid','module_principals','module_menus'
        ));
    }

    public function update(Request $request)
    {
        $audits = new Audits;

        $preguntas = Preguntas::find($request->id);
        $preguntas->description = $request->description;
        $preguntas->tipo=$request->tipo;
        $preguntas->id_tipo_evento=$request->tipo_evento;
        $preguntas->orden = $request->orden;
        $preguntas->save();

        PreguntasRespuestas::where('id_preguntas', $request->id)->delete();

        if($request->tipo=='Cerrada')
        {
            foreach ($request->respuestas as $respuesta) {
                PreguntasRespuestas::create([
                    'id_preguntas' => $preguntas->id,
                    'id_respuestas' => $respuesta
                ]);
            }
        }

        $audits->save_audits('Modify a Preguntas:'.$request->id." - ".$request->description);
        return redirect('preguntas');
    }

    public function updatetipo(Request $request)
    {
        $audits = new Audits;

        $preguntas = Preguntas::find($request->id);
        $preguntas->description = $request->description;
        $preguntas->tipo=$request->tipo;
        $preguntas->id_tipo_evento=$request->tipo_evento;
        $preguntas->orden = $request->orden;
        $preguntas->save();

        PreguntasRespuestas::where('id_preguntas', $request->id)->delete();

        if($request->tipo=='Cerrada')
        {
            foreach ($request->respuestas as $respuesta) {
                PreguntasRespuestas::create([
                    'id_preguntas' => $preguntas->id,
                    'id_respuestas' => $respuesta
                ]);
            }
        }

        $audits->save_audits('Modify a Preguntas:'.$request->id." - ".$request->description);
        return redirect()->action(
            'TipoEventosController@edit', ['id' => $request->tipo_evento]
        );
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $preguntas = Preguntas::find($request->id);
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $preguntasrespuestas = \DB::table('preguntas_respuestas')
            ->where('id_preguntas', '=', $request->id)->pluck('id_respuestas');
        $respuestas = \DB::table('respuestas')->pluck('description', 'id');

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('preguntas.del',
            compact('preguntas','preguntasrespuestas',
                'respuestas','tipo_eventos','module_principals','module_menus'));

    }

    public function deletetipo(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $preguntas = Preguntas::find($request->id);
        $tipo_eventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $preguntasrespuestas = \DB::table('preguntas_respuestas')
            ->where('id_preguntas', '=', $request->id)->pluck('id_respuestas');
        $respuestas = \DB::table('respuestas')->pluck('description', 'id');
        $tipo_eventosid = TipoEventos::find($request->idtipoevento);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('tipoeventos.deltipo', compact('preguntas','preguntasrespuestas',
            'respuestas','tipo_eventos','tipo_eventosid',
          'module_principals','module_menus'));

    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        PreguntasRespuestas::where('id_preguntas', $request->id)->delete();

        Preguntas::find($request->id)->delete();

        $audits->save_audits('Delete a Preguntas:'.$request->id." - ".$request->name);

        return redirect('preguntas');
    }

    public function destroytipo(Request $request)
    {
        $audits = new Audits;

        PreguntasRespuestas::where('id_preguntas', $request->id)->delete();

        Preguntas::find($request->id)->delete();

        $audits->save_audits('Delete a Preguntas:'.$request->id." - ".$request->name);

        return redirect()->action(
            'TipoEventosController@edit', ['id' => $request->tipo_evento]
        );
    }
}
