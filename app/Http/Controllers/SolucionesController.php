<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Soluciones;
use App\Modules;
use App\SolucionesTipos;

class SolucionesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $soluciones = Soluciones::all();


        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('soluciones.index', compact('soluciones', 'user_access',
            'module_principals','module_menus'));
    }

    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('soluciones.add',compact('module_principals','module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;
        $soluciones = Soluciones::create(['description'=>$request->description]);
        $audits->save_audits('Add new Soluciones:'.$soluciones->id." - ".$request->name);
        return redirect('soluciones');
    }

    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = "soluciones";
        $user_access = $module->accesos($iduser, $url);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $soluciones = Soluciones::find($request->id);
        $solucionestipos = SolucionesTipos::where('id_soluciones',$request->id)->get();
        return view('soluciones.mod', compact('soluciones',
            'solucionestipos','user_access','module_principals','module_menus'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;

        $soluciones = Soluciones::find($request->id);
        $soluciones->description = $request->description;
        $soluciones->save();
        $audits->save_audits('Modify a Soluciones:'.$request->id." - ".$request->description);
        return redirect('soluciones');
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $soluciones = Soluciones::find($request->id);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('soluciones.del', compact('soluciones',
            'module_principals','module_menus','module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        Soluciones::find($request->id)->delete();

        $audits->save_audits('Delete a Soluciones:'.$request->id." - ".$request->name);

        return redirect('soluciones');
    }
}
