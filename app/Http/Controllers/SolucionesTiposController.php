<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\SolucionesTipos;
use App\Soluciones;
use App\Audits;
use App\Modules;


class SolucionesTiposController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function news(Request $request)
    {
        $audits = new Audits;

        $solucionestipos = SolucionesTipos::create(
            ['description'=>$request->descriptiontipo,
            'id_soluciones'=>$request->id_sol]
        );

        $audits->save_audits('Add new SolucionesTipos:'.$solucionestipos->id." - ".$request->descriptiontipo);

        return redirect()->action(
            'SolucionesController@edit', ['id' => $request->id_sol]
        );
    }

    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $soluciones = Soluciones::find($request->id_sol);
        $solucionestipos = SolucionesTipos::find($request->id);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('solucionestipo.mod', compact('soluciones','solucionestipos','module_principals','module_menus'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;

        $soluciones = SolucionesTipos::find($request->id_tipo);
        $soluciones->description = $request->description;
        $soluciones->save();
        $audits->save_audits('Modify a SolucionesTipos:'.$request->id_tipo." - ".$request->description);

        return redirect()->action(
            'SolucionesController@edit', ['id' => $request->id]
        );
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $soluciones = Soluciones::find($request->id_sol);
        $solucionestipos = SolucionesTipos::find($request->id);


        return view('solucionestipo.del', compact('soluciones','solucionestipos','module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        SolucionesTipos::find($request->id_tipo)->delete();

        $audits->save_audits('Delete a SolucionesTipos:'.$request->id." - ".$request->name);

        return redirect()->action(
            'SolucionesController@edit', ['id' => $request->id]
        );
    }
}
