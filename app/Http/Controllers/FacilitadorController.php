<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Facilitador;
use App\Modules;

class FacilitadorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $facilitador = \DB::table('facilitador')
            ->select('facilitador.fname','facilitador.lname',
                'facilitador.email','facilitador.id','pais.description')
            ->orderBy('id', 'asc')
            ->join('pais','facilitador.id_pais','=','pais.id')
            ->paginate(10);

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('facilitador.index', compact('facilitador', 'user_access'
        , 'module_principals','module_menus'));
    }

    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $pais = \DB::table('pais')->pluck('description', 'id');
        return view('facilitador.add',compact('pais','facilitador', 'user_access', 'module_principals','module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;
        $facilitador = Facilitador::create(
            [
                'lname'=>$request->lname,
                'fname'=>$request->fname,
                'email'=>$request->email,
                'id_pais'=>$request->pais
            ]
        );
        $audits->save_audits('Add new Facilitador:'.$facilitador->id." - ".$request->name);
        return redirect('facilitador');
    }

    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $pais = \DB::table('pais')->pluck('description', 'id');
        $facilitador = Facilitador::find($request->id);
        return view('facilitador.mod', compact('facilitador','pais', 'module_principals','module_menus'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;
        $facilitador = Facilitador::find($request->id);
        $facilitador->fname = $request->fname;
        $facilitador->lname = $request->lname;
        $facilitador->email = $request->email;
        $facilitador->id_pais = $request->pais;
        $facilitador->save();
        $audits->save_audits('Modify a Facilitador:'.$request->id." - ".$request->description);
        return redirect('facilitador');
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $pais = \DB::table('pais')->pluck('description', 'id');
        $facilitador = Facilitador::find($request->id);
        return view('facilitador.del', compact('facilitador','pais', 'module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        Facilitador::find($request->id)->delete();

        $audits->save_audits('Delete a Facilitador:'.$request->id." - ".$request->name);

        return redirect('facilitador');
    }


}
