<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\TipoEventos;
use App\Modules;



class TipoEventosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $tipoeventos = TipoEventos::all();

        $m = new Modules;
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);
        return view('tipoeventos.index', compact('tipoeventos', 'user_access','module_principals','module_menus'));
    }

    public function add(Request $request)
    {
        $m = new Modules;
        $iduser = \Auth::id();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('tipoeventos.add',compact('module_principals','module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;
        $tipoeventos = TipoEventos::create(['description'=>$request->description]);
        $audits->save_audits('Add new TipoEventos:'.$tipoeventos->id." - ".$request->name);
        return redirect('tipoeventos');
    }

    public function edit(Request $request)
    {
        $m = new Modules;
        
        $tipoeventos = TipoEventos::find($request->id);
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $m->accesos($iduser, $url);

        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $preguntas = \DB::table('preguntas')
            ->select('description','id','orden')
            ->where('id_tipo_evento',$request->id)
            ->orderBy('id', 'asc')->get();

        return view('tipoeventos.mod', compact('tipoeventos','preguntas','module_principals','module_menus','user_access'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;
        $tipoeventos = TipoEventos::find($request->id);
        $tipoeventos->description = $request->description;
        $tipoeventos->save();
        $audits->save_audits('Modify a TipoEventos:'.$request->id." - ".$request->description);
        return redirect('tipoeventos');
    }

    public function delete(Request $request)
    {

      $iduser = \Auth::id();
      $m = new Modules;
      $module_principals = $m->get_modules_principal_user($iduser);
      $module_menus = $m->get_modules_menu_user($iduser);

        $tipoeventos = TipoEventos::find($request->id);
        return view('tipoeventos.del', compact('tipoeventos','module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        TipoEventos::find($request->id)->delete();

        $audits->save_audits('Delete a TipoEventos:'.$request->id." - ".$request->name);

        return redirect('tipoeventos');
    }
}
