<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Eventos;
use App\TipoEventos;
use App\Pais;
use App\Soluciones;
use App\Industrias;
use App\Facilitador;
use App\Modules;
use Carbon\Carbon;
use App\EventosPreguntas;
use App\Respuestas;
use App\EventosPreguntasRespuestasCerradas;
use App\EventosPreguntasRespuestasAbiertas;
use App\EventosFacilitador;
use App\PreguntasRespuestas;

class EventosController extends Controller
{
    /**
     * EventosController constructor.
     */

    public function __construct()
    {
        $this->middleware('auth')->except(['general','encuesta']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        $eventos = \DB::table('eventos')
            ->select('*')
            ->orderBy('id', 'desc')
            ->paginate(5);
        return view('eventos.index', compact('eventos', 'user_access','module_menus','module_principals'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function add(Request $request)
    {
        $facil = new Facilitador;
        $module = new Modules;
        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $industrias = \DB::table('industrias')->pluck('description', 'id');
        $pais = \DB::table('pais')->pluck('description', 'id');
        $facilitador = $facil->todos_facilitador();
        $soluciones = \DB::table('soluciones')->pluck('description', 'id');
        $preguntas = \DB::table('preguntas')->where('id_tipo_evento','1')
            ->pluck('description', 'id');

        return view('eventos.add', compact('tipoeventos',
            'soluciones', 'pais', 'industrias', 'facilitador', 'preguntas','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function news(Request $request)
    {
        $audits = new Audits;
        $iduser = \Auth::id();

        $link = hash('md5', uniqid('142857',true));

        $eventos = Eventos::create([
            'description' => $request->description,
            'organizacion' => $request->organization,
            'id_user' => $iduser,
            'id_pais' => $request->pais,
            'id_industrias' => $request->industria,
            'id_tipo_evento' => $request->tipoevento,
            'id_solucion' => $request->solucion,
            'participantes' => $request->participantes,
            'asistentes' => $request->asistente,
            'fecha_evento' => $request->fecha,
            'fecha_final' => $request->fecha_final,
            'lugar' => $request->lugar,
            'link' => $link
        ]);

        foreach ($request->preguntas as $pregunta) {
            EventosPreguntas::create([
                    'id_preguntas' => $pregunta,
                    'id_eventos' => $eventos->id
                ]);
        }

        foreach ($request->facilitador as $facilitado) {
            EventosFacilitador::create([
                'id_facilitador' => $facilitado,
                'id_eventos' => $eventos->id
            ]);
        }
        $audits->save_audits('Add new Evento:'.$eventos->id." - ".$request->description);

        return redirect('eventos');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Request $request)
    {
        $facil = new Facilitador;

        $module = new Modules;
        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);


        $eventos = Eventos::find($request->id);
        $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $industrias = \DB::table('industrias')->pluck('description', 'id');
        $pais = \DB::table('pais')->pluck('description', 'id');
        $facilitador = $facil->todos_facilitador();

        $eventosfacilitador = \DB::table('eventos_facilitador')->pluck('id_facilitador');

        $soluciones = \DB::table('soluciones')->pluck('description', 'id');
        $preguntas = \DB::table('preguntas')
            ->where('id_tipo_evento',$eventos->id_tipo_evento)
            ->pluck('description', 'id');

        $eventospreguntas = \DB::table('eventos_preguntas')
            ->where('id_eventos', '=', $request->id)->pluck('id_preguntas');

        return view('eventos.mod',
            compact('eventos', 'tipoeventos', 'soluciones', 'pais', 'industrias',
                'facilitador', 'preguntas', 'eventospreguntas','eventosfacilitador',
                'module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request)
    {
        $audits = new Audits;
        $eventos = Eventos::find($request->id);
        $eventos->description = $request->description;
        $eventos->organizacion = $request->organization;
        $eventos->lugar = $request->lugar;
        $eventos->id_pais = $request->pais;
        $eventos->id_industrias = $request->industria;
        $eventos->id_solucion = $request->solucion;
        $eventos->id_tipo_evento = $request->tipoevento;
        $eventos->asistentes = $request->asistente;
        $eventos->fecha_final = $request->fecha_final;
        $eventos->fecha_evento = $request->fecha;
        $eventos->save();

        EventosPreguntas::where('id_eventos', $request->id)->delete();

        foreach ($request->preguntas as $pregunta) {
            EventosPreguntas::create([
                'id_preguntas' => $pregunta,
                'id_eventos' => $eventos->id
            ]);
        }

        EventosFacilitador::where('id_eventos', $request->id)->delete();

        foreach ($request->facilitador as $facilitado) {
            EventosFacilitador::create([
                'id_facilitador' => $facilitado,
                'id_eventos' => $eventos->id
            ]);
        }

        $audits->save_audits('Modify a Evento:'.$request->id." - ".$request->description);
        return redirect('eventos');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function delete(Request $request)
    {
        $facil = new Facilitador;

        $module = new Modules;
        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);


        $eventos = Eventos::find($request->id);
        $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $industrias = \DB::table('industrias')->pluck('description', 'id');
        $pais = \DB::table('pais')->pluck('description', 'id');
        $facilitador = $facil->todos_facilitador();

        $eventosfacilitador = \DB::table('eventos_facilitador')->pluck('id_facilitador');

        $soluciones = \DB::table('soluciones')->pluck('description', 'id');
        $preguntas = \DB::table('preguntas')
            ->where('id_tipo_evento',$eventos->id_tipo_evento)
            ->pluck('description', 'id');

        $eventospreguntas = \DB::table('eventos_preguntas')
            ->where('id_eventos', '=', $request->id)->pluck('id_preguntas');

        return view('eventos.del',
            compact('eventos', 'tipoeventos', 'soluciones', 'pais', 'industrias',
                'facilitador', 'preguntas', 'eventospreguntas','eventosfacilitador',
                'module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy(Request $request)
    {
        $audits = new Audits;
        $audits->save_audits('Delete a Eventos:'.$request->id." - ".$request->description);

        EventosPreguntas::where('id_eventos', $request->id)->delete();
        EventosFacilitador::where('id_eventos', $request->id)->delete();
        Eventos::find($request->id)->delete();

        return redirect('eventos');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function encuesta(Request $request)
    {
        $evento_link = new Eventos;
        $facil = new Facilitador;
        $resp = new Respuestas;

        $EventosPreguntas = new EventosPreguntas;

        $eventos = $evento_link->buscar_link($request->id);

        if($eventos->count()>0) {
            foreach ($eventos as $evento) {
                $fecha_final_evento = $evento->fecha_final;
                $eventos = Eventos::find($evento->id);
            }
            $fecha_actual = date('Y-m-d');


            if ($fecha_final_evento >= $fecha_actual) {
                $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
                $industrias = \DB::table('industrias')->pluck('description', 'id');
                $pais = \DB::table('pais')->pluck('description', 'id');

                $facilitador = $facil->todos_facilitador_encuenta($eventos->id)->pluck('name', 'id');

                $soluciones = \DB::table('soluciones')->pluck('description', 'id');
                $preguntas = $EventosPreguntas->get_eventos_preguntas($eventos->id);

                $respuestas = $resp->ListDescriptionValue()->pluck('description', 'id');

                return view('eventos.encuesta', compact(
                    'eventos', 'tipoeventos', 'soluciones', 'pais',
                    'industrias', 'facilitador', 'preguntas', 'respuestas'
                ));

            } else {

                return view('eventos.error');
            }
        }else{
                return view('eventos.error');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function encuestasave(Request $request)
    {
    
        $audits = new Audits;

        $EventosPreguntas = new EventosPreguntas;
        $eventos = new Eventos;

        $eventos_preguntas = $EventosPreguntas->get_eventos_preguntas($request->id);



        foreach ($eventos_preguntas as $eventos_pregunta)
        {
            $resp='pre'.$eventos_pregunta->id;
            $resp_value="";
            $resp_value = $request->$resp;

            if($eventos_pregunta->tipo=="Cerrada")
            {
                EventosPreguntasRespuestasCerradas::create([
                    'id_preguntas' => $eventos_pregunta->id,
                    'id_eventos' => $eventos_pregunta->id_eventos,
                    'id_respuestas'=>$resp_value
                ]);
            }

            if(isset($request->autorizar))
            {
                $authorize=$request->autorizar;
            }
            else
            {
                $authorize=0;
            }
            
            if($eventos_pregunta->tipo=="Abierta")
            {
                EventosPreguntasRespuestasAbiertas::create([
                    'id_preguntas' => $eventos_pregunta->id,
                    'id_eventos' => $eventos_pregunta->id_eventos,
                    'respuesta'=>$resp_value,
                    'authorize'=>$authorize
                ]);
            }
            $resp_value="";
        }

        $eventos->participante($request->id);

        $audits->save_audits('Encuesta Realizada:'.$request->id);

        return view('eventos.encuestaterminada');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function ver(Request $request)
    {
        return redirect()->route('/eventos/encuesta', ['id' => $request->id]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function general(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $m = new Modules();
        $module_principals = $m->get_modules_principal_user($iduser);
        $module_menus = $m->get_modules_menu_user($iduser);

        return view('eventos.general', compact('user_access','module_menus','module_principals'));
    }
}
