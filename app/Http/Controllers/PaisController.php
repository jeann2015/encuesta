<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Pais;
use App\Modules;

class PaisController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

  public function index(Request $request)
  {
      $module = new Modules;
      $iduser = \Auth::id();
      $url = $request->path();
      $user_access = $module->accesos($iduser, $url);
      $pais = Pais::all();

      $m = new Modules();
      $module_principals = $m->get_modules_principal_user($iduser);
      $module_menus = $m->get_modules_menu_user($iduser);

      return view('pais.index', compact('pais', 'user_access','module_principals','module_menus'));
  }

  public function add(Request $request)
  {
      $iduser = \Auth::id();

      $m = new Modules();
      $module_principals = $m->get_modules_principal_user($iduser);
      $module_menus = $m->get_modules_menu_user($iduser);

      return view('pais.add',compact('module_principals','module_menus'));
  }

  public function news(Request $request)
  {
      $audits = new Audits;

      $pais = Pais::create([
          'description'=>$request->description,
          'iniciales'=>$request->iniciales
          ]);

      $audits->save_audits('Add new Pais:'.$pais->id." - ".$request->description);
      return redirect('pais');
  }

  public function edit(Request $request)
  {
      $m = new Modules();
      $iduser = \Auth::id();
      $module_principals = $m->get_modules_principal_user($iduser);
      $module_menus = $m->get_modules_menu_user($iduser);

      $pais = Pais::find($request->id);
      return view('pais.mod', compact('pais','module_principals','module_menus'));
  }

  public function update(Request $request)
  {
      $audits = new Audits;

      $pais = Pais::find($request->id);
      $pais->description = $request->description;
      $pais->iniciales = $request->iniciales;
      $pais->save();

      $audits->save_audits('Modify a Pais:'.$request->id." - ".$request->description);
      return redirect('pais');
  }

  public function delete(Request $request)
  {
      $m = new Modules();
      $iduser = \Auth::id();
      $module_principals = $m->get_modules_principal_user($iduser);
      $module_menus = $m->get_modules_menu_user($iduser);

      $pais = Pais::find($request->id);
      return view('pais.del', compact('pais','module_principals','module_menus'));
  }

  public function destroy(Request $request)
  {
      $audits = new Audits;

      Pais::find($request->id)->delete();

      $audits->save_audits('Delete a Pais:'.$request->id." - ".$request->name);

      return redirect('pais');
  }
}
