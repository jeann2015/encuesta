<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Eventos;
use App\Facilitador;
use App\Modules;
use Carbon\Carbon;
use App\EventosPreguntas;
use App\EventosPreguntasRespuestasAbiertas;
use Mail;
use Illuminate\Support\Facades\Session;


class ReportsController extends Controller
{

    /**
     * ReportsController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function especifico(Request $request)
    {
        $module = new Modules;
        $facil = new Facilitador;

        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $tipoevento = \DB::table('tipo_evento')->pluck('description', 'id')->put('0','Todos');
        $tipoeventos = array_sort_recursive($tipoevento->toArray());


        $industria = \DB::table('industrias')->pluck('description', 'id')->put('0','Todos');
        $industrias = array_sort_recursive($industria->toArray());

        $pai = \DB::table('pais')->pluck('description', 'id')->put('0','Todos');
        $pais = array_sort_recursive($pai->toArray());

        $facilitado = $facil->todos_facilitador()->put('0','Todos');
        $facilitador = array_sort_recursive($facilitado->toArray());

        $solucione = \DB::table('soluciones')->pluck('description', 'id')->put('0','Todos');
        $soluciones = array_sort_recursive($solucione->toArray());


        $values=collect([
        'pais'=>0,
            'tipoeventos'=>0,
            'industrias'=>0,
            'soluciones'=>0,
            'facilitador'=>0,
            'desde'=> Carbon::now()->toDateString(),
            'hasta'=> Carbon::now()->toDateString()
        ]);

        return view('reports.especifico',
            compact( 'tipoeventos','user_access',
            'pais','soluciones','facilitador','industrias','values',
                'module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function espsearch(Request $request)
    {
        $module = new Modules;
        $facil = new Facilitador;
        $events = new Eventos;

        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        Carbon::setLocale(config('app.locale'));


        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $tipoevento = \DB::table('tipo_evento')->pluck('description', 'id')->put('0','Todos');
        $tipoeventos = array_sort_recursive($tipoevento->toArray());


        $industria = \DB::table('industrias')->pluck('description', 'id')->put('0','Todos');
        $industrias = array_sort_recursive($industria->toArray());

        $pai = \DB::table('pais')->pluck('description', 'id')->put('0','Todos');
        $pais = array_sort_recursive($pai->toArray());

        $facilitado = $facil->todos_facilitador()->put('0','Todos');
        $facilitador = array_sort_recursive($facilitado->toArray());

        $solucione = \DB::table('soluciones')->pluck('description', 'id')->put('0','Todos');
        $soluciones = array_sort_recursive($solucione->toArray());

        $eventos = $events->SearchEvent(
                $request->pais,
                $request->tipoevento,
                $request->solucion,
                $request->industria,
                $request->facilitador,
                $request->desde,
                $request->hasta,0);

        $values=collect([
            'pais'=>$request->pais,
            'tipoeventos'=>$request->tipoevento,
            'industrias'=>$request->industria,
            'soluciones'=>$request->solucion,
            'facilitador'=>$request->facilitador,
            'desde'=> $request->desde,
            'hasta'=> $request->hasta
        ]);


        return view('reports.especifico',
            compact( 'tipoeventos','user_access',
            'pais','preguntas','soluciones','facilitador',
            'industrias','eventos','values',
                'module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function espver(Request $request)
    {
        $module = new Modules;
        $events = new Eventos;

        $iduser = \Auth::id();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        Carbon::setLocale(config('app.locale'));

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);

        $eventos = $events->SearchEventId($request->id);

        return view('reports.verespecifico', compact( 'eventos','module_principals','module_menus'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function espgraph(Request $request)
    {
        $module = new Modules;
        $events = new Eventos;

        $questions = new EventosPreguntas;
        $EventosPreguntasRespuestasAbiertas = new EventosPreguntasRespuestasAbiertas;

        $id_event = $request->id;

        $eventos = $events
            ->SearchEventId($request->id);

        $preguntas_abiertas = $questions
            ->get_eventos_preguntas_graph($request->id,'Abierta');

        $preguntas_cerrada = $questions
            ->get_eventos_preguntas_graph($request->id,'Cerrada');
        $respuestas_abiertas = $EventosPreguntasRespuestasAbiertas
            ->get_eventos_preguntas_respuestas_graph($request->id);


        if($request->opciones==2)
        {
            Mail::raw('Reporte Especifico', function ($message) use($request){
                $message->from("jeancarlosn2008@gmail.com", $name = null);
                $message->subject("Reporte Especifico");
                $message->to($request->email, $name = null);
                $message->attach( $request->archivo->getRealPath(), array(
                        'as' => 'archivo.' . $request->archivo->getClientOriginalExtension(),
                        'mime' => $request->archivo->getMimeType())
                );

            });

            Session::flash('message', 'Hemos Enviado a su Correo el Reporte!');

            return redirect()->action(
                'ReportsController@espver', ['id' => $id_event]
            );
        }
        else
        {
            return view('reports.graphespecifico', compact( 'eventos',
            'preguntas_abiertas','preguntas_cerrada','respuestas_abiertas'));
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function consolidado(Request $request)
    {
        $module = new Modules;
        $facil = new Facilitador;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $facilitador = $facil->todos_facilitador();

        $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $industrias = \DB::table('industrias')->pluck('description', 'id');
        $pais = \DB::table('pais')->pluck('description', 'id');
        $soluciones = \DB::table('soluciones')->pluck('description', 'id');
        $preguntas = \DB::table('preguntas')->pluck('description', 'id');

        return view('reports.consolidado', compact( 'tipoeventos','user_access',
            'pais','preguntas','soluciones','facilitador','industrias'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function periodo(Request $request)
    {
        $module = new Modules;
        $facil = new Facilitador;

        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $facilitador = $facil->todos_facilitador();

        $tipoeventos = \DB::table('tipo_evento')->pluck('description', 'id');
        $industrias = \DB::table('industrias')->pluck('description', 'id');
        $pais = \DB::table('pais')->pluck('description', 'id');
        $soluciones = \DB::table('soluciones')->pluck('description', 'id');
        $preguntas = \DB::table('preguntas')->pluck('description', 'id');

        return view('reports.periodo', compact( 'tipoeventos','user_access',
            'pais','preguntas','soluciones','facilitador','industrias'));
    }
}
