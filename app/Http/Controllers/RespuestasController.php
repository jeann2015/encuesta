<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Audits;
use App\Respuestas;
use App\Modules;

class RespuestasController extends Controller
{
    /**
     * RespuestasController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $user_access = $module->accesos($iduser, $url);
        $respuestas = Respuestas::all();

        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);

        return view('respuestas.index', compact('respuestas', 'user_access',
            'module_principals','module_menus'));
    }

    public function add(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $user_access = $module->accesos($iduser, $url);

        return view('respuestas.add',compact( 'user_access','module_principals','module_menus'));
    }

    public function news(Request $request)
    {
        $audits = new Audits;
        $respuestas = Respuestas::create(['description'=>$request->description,'valor'=>$request->valor]);
        $audits->save_audits('Add new Respuestas:'.$respuestas->id." - ".$request->name);
        return redirect('respuestas');
    }

    public function edit(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $user_access = $module->accesos($iduser, $url);

        $respuestas = Respuestas::find($request->id);
        return view('respuestas.mod', compact('respuestas','user_access','module_principals','module_menus'));
    }

    public function update(Request $request)
    {
        $audits = new Audits;
        $respuestas = Respuestas::find($request->id);
        $respuestas->description = $request->description;
        $respuestas->valor = $request->valor;
        $respuestas->save();
        $audits->save_audits('Modify a Respuestas:'.$request->id." - ".$request->description);
        return redirect('respuestas');
    }

    public function delete(Request $request)
    {
        $module = new Modules;
        $iduser = \Auth::id();
        $url = $request->path();
        $module_principals = $module->get_modules_principal_user($iduser);
        $module_menus = $module->get_modules_menu_user($iduser);
        $user_access = $module->accesos($iduser, $url);

        $respuestas = Respuestas::find($request->id);
        return view('respuestas.del', compact('respuestas',
            'user_access','module_principals','module_menus'));
    }

    public function destroy(Request $request)
    {
        $audits = new Audits;

        Respuestas::find($request->id)->delete();

        $audits->save_audits('Delete a Respuestas:'.$request->id." - ".$request->description);

        return redirect('respuestas');
    }
}
