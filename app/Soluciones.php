<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Soluciones extends Model
{
    protected $table="soluciones";
    protected $fillable = [
          'description'
      ];

}
