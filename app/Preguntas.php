<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Preguntas extends Model
{
    protected $table="preguntas";
    protected $fillable = [
        'description',
        'tipo',
        'id_tipo_evento',
        'orden'
    ];
}
