<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventosFacilitador extends Model
{
    protected $table="eventos_facilitador";

    protected $fillable = [
        'id_eventos',
        'id_facilitador'
    ];
}
