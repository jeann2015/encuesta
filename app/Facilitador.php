<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Facilitador extends Model
{
    protected $table="facilitador";
    protected $fillable = [
          'fname',
          'lname',
          'email',
          'id_pais'

      ];

    public function todos_facilitador()
    {
        $facilitador = \DB::table('facilitador')
        ->select(
            ['facilitador.id',
        \DB::raw("concat(facilitador.fname,' ',facilitador.lname,'  (',pais.iniciales,')') as name")
        ])
        ->join('pais','facilitador.id_pais','=','pais.id')
        ->orderBy('id', 'asc')->pluck('name','id');

        return $facilitador;
    }

    public function todos_facilitador_encuenta($id_eventos)
    {
        $facilitador = \DB::table('facilitador')
        ->select(
            ['facilitador.id',
        \DB::raw("concat(facilitador.fname,' ',facilitador.lname,' (',pais.iniciales,')') as name")
        ])
        ->join('pais','facilitador.id_pais','=','pais.id')
        ->join('eventos_facilitador','eventos_facilitador.id_facilitador','=','facilitador.id')
        ->where('eventos_facilitador.id_eventos','=',$id_eventos)
        ->orderBy('id', 'asc')->get();

        return $facilitador;
    }
}
