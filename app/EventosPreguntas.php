<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection as Collection;

class EventosPreguntas extends Model
{
    protected $table="eventos_preguntas";

    protected $fillable = [
        'id_preguntas',
        'id_eventos'
    ];

    public function get_eventos_preguntas($idevento)
    {
        $module_principal = \DB::table('eventos_preguntas')
            ->select('preguntas.description',
                'eventos_preguntas.id_preguntas as id','preguntas.tipo',
                'eventos_preguntas.id_eventos')
            ->join('preguntas', function ($join) use ($idevento) {
                $join->on('eventos_preguntas.id_preguntas', '=', 'preguntas.id')
                    ->where('eventos_preguntas.id_eventos', '=', $idevento);
            })
            ->orderBy('eventos_preguntas.id_preguntas')->get();

        return  Collection::make($module_principal);
    }

    public function get_eventos_preguntas_graph($idevento,$tipo)
    {
        $module_principal = \DB::table('eventos_preguntas')
            ->select('preguntas.description','eventos_preguntas.id_preguntas as id','preguntas.tipo')
            ->join('preguntas', function ($join) use ($idevento) {
                $join->on('eventos_preguntas.id_preguntas', '=', 'preguntas.id')
                    ->where('eventos_preguntas.id_eventos', '=', $idevento);
            })
            ->where('preguntas.tipo','=',$tipo)
            ->orderBy('eventos_preguntas.id_preguntas')->get();

        return  Collection::make($module_principal);
    }

    public function get_preguntasRespuestas_eventos($id)
    {

        $module_principal = \DB::table('preguntas_respuestas')
            ->select('respuestas.id',
                'respuestas.description',
                'preguntas_respuestas.id_preguntas')
            ->join('respuestas', function($join) {
                $join->on('preguntas_respuestas.id_respuestas', '=', 'respuestas.id');
            })
            ->join('eventos_preguntas', function($join) use($id){
                $join->on('eventos_preguntas.id_preguntas', '=', 'preguntas_respuestas.id_preguntas')
                    ->where('eventos_preguntas.id_eventos','=',$id);
            })->get();

        return  $module_principal;

    }
}
